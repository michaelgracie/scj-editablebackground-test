﻿var current_presentation_id = null;
var crm_allow_save_data = false;
var crm_current_call_key = '';
var crm_current_presentation_key = '';
var pitch_data;

setupLayoutSetting();

var showLogErrors = true; //Adds error logging button, tabs and slide

(function () {
    var testing = (location.search === '');
    var include = function (file) {
        var tag = '<script src="' + file + '"></script>';
        console.log('writing script tag: ' + tag);
        document.write(tag);
    };

    function testingStartup() {

        var data = {

            ARTS_Client_Activities: [{ "id": "18", "dept": "13 Household Chemicals", "ord": "5", "distribution_folder": { "153044": { "codes": "|046500772337|10046500772334|0004650077233| 046500772337||046500772337|004650077233|~|45708|~|405565608621|", "PID": "153044", "CM": "192", "IS": " ", "wf_r": "", "OS": { "is_positive": false, "id": "26", "pt": "Low Inventory (LI)" }, "PF": "192", "CL": "62", "CPT": "Air Sanitizers - Fresheners - Candles", "oth_info": " |NA|1/30/2018~SH~CF~8/14/2017~SH~CF~3/7/2017~SH~CF~~S~S~|04.2Z|06|SC Johnson Household|77233||| |Yes|PIC|2018/06/04 12:15:42~NA~NIS|| |", "PB": "130", "url": "", "WF": true, "WD": true, "Com": true, "NI": false, "AI": true, "ZS": false, "SC": false, "bad": false, "AT": false, "WF2": false, "invres": null, "aux": { "Fields": {} }, "wf_sr": "Auto", "id": "828857647", "prompt": "Glade Candle NO 2 Sweet Pea & Pear 4.2z", "is_modified": true, "last_updated": "2018/11/01 09:11:13", "AC": { "id": "6", "pt": "Filled (F)" }, "RS": { "is_positive": true, "id": "3", "pt": "On Shelf (SH)" } }, "153045": { "codes": "|046500772344|10046500772341|0004650077234| 046500772344||046500772344|004650077234|~|45707|~|405565608607|", "PID": "153045", "CM": "192", "IS": " ", "wf_r": "", "OS": { "is_positive": false, "id": "1", "pt": "Out-Of-Stock (OOS)" }, "PF": "192", "CL": "62", "CPT": "Air Sanitizers - Fresheners - Candles", "oth_info": " |NA|1/30/2018~SH~CF~8/14/2017~SH~CF~3/7/2017~SH~CF~~S~S~|04.2Z|06|SC Johnson Household|77234||| |Yes|PIC|2018/06/04 12:15:46~NA~NIS|| |", "PB": "130", "url": "", "WF": true, "WD": true, "Com": false, "NI": false, "AI": true, "ZS": false, "SC": false, "bad": true, "AT": false, "WF2": false, "invres": null, "aux": { "Fields": { "aux5": "1" } }, "wf_sr": "Auto", "id": "828857495", "prompt": "Glade Candle NO 3 Coconut & Beachwoods 4.2z", "is_modified": true, "last_updated": "2018/11/01 09:11:33", "AC": null, "is_modified_aux": true }, "153049": { "codes": "|046500772283|10046500772280|0004650077228| 046500772283||046500772283|004650077228|~|45713|~|405565608676|", "PID": "153049", "CM": "192", "IS": " ", "wf_r": "", "OS": { "is_positive": false, "id": "26", "pt": "Low Inventory (LI)" }, "PF": "195", "CL": "62", "CPT": "Air Sanitizers - Fresheners - Candles", "oth_info": " |NA|5/22/2018~NA~NOP~4/4/2018~NA~NOP~3/6/2018~NA~NOP~~~~|01.5Z|04|SC Johnson Household|77228||| |Yes|PIC|2018/06/04 12:15:20~NA~NIS|| |", "PB": "130", "url": "", "WF": true, "WD": true, "Com": false, "NI": false, "AI": true, "ZS": false, "SC": false, "bad": true, "AT": false, "WF2": false, "invres": null, "aux": { "Fields": { "aux5": "2" } }, "wf_sr": "Auto", "id": "828866962", "prompt": "Glade PISO NO 2- 2 Refills Sweet Pea & Pear 1.5z", "is_modified": true, "last_updated": "2018/11/01 09:11:46", "AC": null, "is_modified_aux": true }, "172631": { "codes": "|046500777745|10046500777742|0004650077774| 046500777745||046500777745|004650077774|~|19388|", "PID": "172631", "CM": null, "IS": "U", "wf_r": "", "OS": { "is_positive": false, "id": "1", "pt": "Out-Of-Stock (OOS)" }, "PF": "195", "CL": "62", "CPT": "Air Sanitizers - Fresheners - Candles", "oth_info": "U|OOS|5/22/2018~NA~NOP~1/30/2018~DV~MNA~~~~~~~|03.35Z|06|SC Johnson Household|77774|||U|No|PIC|2018/06/04 12:16:16~DV~TG||U|", "PB": "130", "url": "", "WF": true, "WD": true, "Com": true, "NI": true, "AI": true, "ZS": false, "SC": false, "bad": true, "AT": true, "WF2": false, "invres": null, "aux": { "Fields": { "aux5": "8" } }, "wf_sr": "New Unconfirmed", "id": "964771484", "prompt": "Glade PISO 5ct Refill Clean Linen", "is_modified_aux": true, "last_updated": "2018/11/01 09:10:27", "is_modified": true, "AC": { "id": "9", "pt": "Adjusted Inventory (AI)" }, "RS": { "is_positive": false, "id": "1", "pt": "Out-Of-Stock (OOS)" } }, "172633": { "codes": "|046500777769|10046500777765|0004650077776| 046500777769||046500777769|004650077776|~|19389|", "PID": "172633", "CM": null, "IS": " ", "wf_r": "", "OS": { "is_positive": false, "id": "1", "pt": "Out-Of-Stock (OOS)" }, "PF": "195", "CL": "62", "CPT": "Air Sanitizers - Fresheners - Candles", "oth_info": " |OOS|5/22/2018~NA~NOP~1/30/2018~DV~MNA~~~~~~~|03.35Z|06|SC Johnson Household|77776||| |Yes|PIC|2018/06/04 12:16:20~DV~TG|| |", "PB": "130", "url": "", "WF": true, "WD": true, "Com": true, "NI": false, "AI": true, "ZS": false, "SC": false, "bad": true, "AT": true, "WF2": false, "invres": null, "aux": { "Fields": { "aux5": "7" } }, "wf_sr": "Auto", "id": "964772393", "prompt": "Glade PISO 5ct Refill Hawaiian Breeze", "is_modified_aux": true, "last_updated": "2018/11/01 09:11:34", "is_modified": true, "AC": { "id": "9", "pt": "Adjusted Inventory (AI)" }, "RS": { "is_positive": false, "id": "1", "pt": "Out-Of-Stock (OOS)" } } }, "tasks": { "folder": [] }, "surveys": { "folder": {} }, "promotions": { "folder": {} }, "pt": "Air Sanitizers - Fresheners - Candles" }, { "id": "39", "dept": "Miscellaneous", "ord": "50", "distribution_folder": {}, "tasks": { "folder": [] }, "surveys": { "folder": { "115601": { "pt": "Food/Target Store Audits", "id": "115601", "folder": [{ "values": [{ "id": "0", "pt": "Yes" }, { "id": "1", "pt": "No" }, { "id": "2", "pt": "NA" }], "id": "296869", "pt": "Did the RSM sign the vendor log on the last call?", "answer": null, "group": "115601", "type": "PICK", "qtype": "YNNA", "last_ans": "", "act": "1", "sbc": "0", "sbs": "", "condition": null, "gq": "324622", "iscon": false, "ASMID": "ASM_296869", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }, { "min": "0", "max": "100", "decimal": false, "after_dec": "0", "id": "296870", "pt": "How many OOS identified during the audit?", "answer": null, "group": "115601", "type": "NUMBER", "qtype": "Numeric", "last_ans": "", "act": "1", "sbc": null, "sbs": null, "condition": null, "gq": null, "iscon": false, "ASMID": "ASM_296870", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }, { "min": "0", "max": "100", "decimal": false, "after_dec": "0", "id": "296871", "pt": "How many OOS were reported on the last call?", "answer": null, "group": "115601", "type": "NUMBER", "qtype": "Numeric", "last_ans": "", "act": "1", "sbc": null, "sbs": null, "condition": null, "gq": null, "iscon": false, "ASMID": "ASM_296871", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }, { "min": "0", "max": "50", "decimal": false, "after_dec": "0", "id": "296872", "pt": "How many secondary displays are on the sales floor?", "answer": null, "group": "115601", "type": "NUMBER", "qtype": "Numeric", "last_ans": "", "act": "1", "sbc": null, "sbs": null, "condition": null, "gq": null, "iscon": false, "ASMID": "ASM_296872", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }, { "values": [{ "id": "0", "pt": "Yes" }, { "id": "1", "pt": "No" }, { "id": "2", "pt": "NA" }], "id": "296873", "pt": "Has the store contacts been updated?", "answer": null, "group": "115601", "type": "PICK", "qtype": "YNNA", "last_ans": "", "act": "1", "sbc": "0", "sbs": "", "condition": null, "gq": "324626", "iscon": false, "ASMID": "ASM_296873", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }, { "values": [{ "id": "0", "pt": "Yes" }, { "id": "1", "pt": "No" }, { "id": "2", "pt": "NA" }], "id": "296874", "pt": "Does store management know the Advantage/ SCJ RSM?", "answer": null, "group": "115601", "type": "PICK", "qtype": "YNNA", "last_ans": "", "act": "1", "sbc": "0", "sbs": "", "condition": null, "gq": "324627", "iscon": false, "ASMID": "ASM_296874", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }, { "min": "0", "max": "50", "decimal": false, "after_dec": "0", "id": "296875", "pt": "ow many task were created during this audit for RSM follow up?", "answer": null, "group": "115601", "type": "NUMBER", "qtype": "Numeric", "last_ans": "", "act": "1", "sbc": null, "sbs": null, "condition": null, "gq": null, "iscon": false, "ASMID": "ASM_296875", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }, { "values": [{ "id": "407056", "pt": "Exceeds" }, { "id": "407057", "pt": "Meets" }, { "id": "407058", "pt": "Below" }], "id": "296876", "pt": "Are store conditions to expectations?", "answer": null, "group": "115601", "type": "PICK", "qtype": "Pick List", "last_ans": "", "act": "1", "sbc": null, "sbs": null, "condition": null, "gq": null, "iscon": false, "ASMID": "ASM_296876", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }, { "id": "296877", "pt": "Follow up notes", "answer": null, "group": "115601", "type": "TEXT", "qtype": "Text", "last_ans": "", "act": "1", "sbc": null, "sbs": null, "condition": null, "gq": null, "iscon": false, "ASMID": "ASM_296877", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }, { "values": [{ "id": "0", "pt": "Yes" }, { "id": "1", "pt": "No" }, { "id": "2", "pt": "NA" }], "id": "296878", "pt": " Are store notes being properly entered in this store?", "answer": null, "group": "115601", "type": "PICK", "qtype": "YNNA", "last_ans": "", "act": "1", "sbc": "0", "sbs": "", "condition": null, "gq": "324631", "iscon": false, "ASMID": "ASM_296878", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }, { "values": [{ "id": "0", "pt": "Yes" }, { "id": "1", "pt": "No" }, { "id": "2", "pt": "NA" }], "id": "296879", "pt": " Is the RS sending an audit follow up email directly after this call and copying ROM?", "answer": null, "group": "115601", "type": "PICK", "qtype": "YNNA", "last_ans": "", "act": "1", "sbc": "0", "sbs": "", "condition": null, "gq": "324632", "iscon": false, "ASMID": "ASM_296879", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }], "cm": "192", "ct": "457", "iscon": false, "atch": "", "URL": "" } } }, "promotions": { "folder": {} }, "pt": "Miscellaneous" }],

            //[{ "id": "134", "dept": "13 Household Chemicals", "ord": "10", "distribution_folder": { "175102": { "codes": "|025700716204|10025700716201|0002570071620| 025700716204||025700716204|002570071620|~|71620|", "PID": "175102", "CM": null, "IS": "U", "wf_r": "", "OS": { "is_positive": false, "id": "1", "pt": "Out-Of-Stock (OOS)" }, "PF": "26", "CL": "62", "CPT": "Bags - Wraps - Foils", "oth_info": "U|OOS|~~~~~~~~~~~~|30CT|09|SC Johnson Household|71620|||U|No|PIC|2018/06/25 11:18:40~DV~TG||U|", "PB": "373", "url": "", "WF": true, "WD": true, "Com": false, "NI": true, "AI": true, "ZS": false, "SC": false, "bad": true, "AT": true, "WF2": false, "invres": null, "aux": { "Fields": { "aux5": "23" } }, "wf_sr": "New Unconfirmed", "id": "1002930384", "prompt": "Ziploc Disney Avengers Slider Storage Qt 30ct", "is_modified_aux": true, "last_updated": "2018/10/19 07:55:13", "is_modified": true }, "175106": { "codes": "|025700716051|10025700716058|0002570071605| 025700716051||025700716051|002570071605|~|71605|", "PID": "175106", "CM": null, "IS": "U", "wf_r": "", "OS": { "is_positive": false, "id": "1", "pt": "Out-Of-Stock (OOS)" }, "PF": "26", "CL": "62", "CPT": "Bags - Wraps - Foils", "oth_info": "U|OOS|~~~~~~~~~~~~|66CT|12|SC Johnson Household|71605|||U|No|PIC|2018/06/25 11:18:36~DV~TG||U|", "PB": "373", "url": "", "WF": true, "WD": true, "Com": false, "NI": true, "AI": true, "ZS": false, "SC": false, "bad": true, "AT": true, "WF2": false, "invres": null, "aux": { "Fields": { "aux5": "20" } }, "wf_sr": "New Unconfirmed", "id": "1002925178", "prompt": "Ziploc Disney Avengers Sandwich 66ct", "is_modified_aux": true, "last_updated": "2018/10/19 07:55:06", "is_modified": true } }, "tasks": { "folder": [] }, "surveys": { "folder": {} }, "promotions": { "folder": {} }, "pt": "Bags - Wraps - Foils" }, { "id": "275", "dept": "Miscellaneous", "ord": "62", "distribution_folder": { "175707": { "codes": "|046500772662|10046500772662|0004650077266| 046500772662||046500772662|004650077266|~|rack|", "PID": "175707", "CM": "192", "IS": " ", "wf_r": "", "OS": { "is_positive": false, "id": "26", "pt": "Low Inventory (LI)" }, "PF": "5158", "CL": "61", "CPT": "Racks/Clipstrips/Fixtures/Supplies", "oth_info": " |OOS|~~~~~~~~~~~~|UNIT|01|SC Johnson Insecticides|77266||| |Yes|PIC|2018/03/26 10:51:00~OOS~ORS|| |", "PB": "226", "url": "", "WF": true, "WD": true, "Com": false, "NI": false, "AI": true, "ZS": false, "SC": false, "bad": true, "AT": true, "WF2": false, "invres": null, "aux": { "Fields": { "aux5": "67" } }, "wf_sr": "Auto", "id": "990437860", "prompt": "OFF Rack", "is_modified": true, "last_updated": "2018/10/19 07:55:41", "AC": null, "is_modified_aux": true }, "175708": { "codes": "|046500772658|10046500772658|0004650077265| 046500772658||046500772658|004650077265|~|raid rack|", "PID": "175708", "CM": "192", "IS": " ", "wf_r": "", "OS": { "is_positive": false, "id": "26", "pt": "Low Inventory (LI)" }, "PF": "5158", "CL": "61", "CPT": "Racks/Clipstrips/Fixtures/Supplies", "oth_info": " |OOS|~~~~~~~~~~~~|UNIT|01|SC Johnson Insecticides|77265||| |Yes|PIC|2018/03/26 10:51:03~OOS~ORS|| |", "PB": "256", "url": "", "WF": true, "WD": true, "Com": false, "NI": false, "AI": true, "ZS": false, "SC": false, "bad": true, "AT": true, "WF2": false, "invres": null, "aux": { "Fields": { "aux5": "78" } }, "wf_sr": "Auto", "id": "990428612", "prompt": "Raid Rack", "is_modified": true, "last_updated": "2018/10/19 07:55:32", "AC": null, "is_modified_aux": true } }, "tasks": { "folder": [] }, "surveys": { "folder": {} }, "promotions": { "folder": {} }, "pt": "Racks/Clipstrips/Fixtures/Supplies" }, { "id": "39", "dept": "Miscellaneous", "ord": "50", "distribution_folder": {}, "tasks": { "folder": [] }, "surveys": { "folder": { "115601": { "pt": "Food/Target Store Audits", "id": "115601", "folder": [{ "values": [{ "id": "0", "pt": "Yes" }, { "id": "1", "pt": "No" }, { "id": "2", "pt": "NA" }], "id": "296869", "pt": "Did the RSM sign the vendor log on the last call?", "answer": null, "group": "115601", "type": "PICK", "qtype": "YNNA", "last_ans": "", "act": "1", "sbc": "0", "sbs": "", "condition": null, "gq": "324622", "iscon": false, "ASMID": "ASM_296869", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }, { "min": "0", "max": "100", "decimal": false, "after_dec": "0", "id": "296870", "pt": "How many OOS identified during the audit?", "answer": null, "group": "115601", "type": "NUMBER", "qtype": "Numeric", "last_ans": "", "act": "1", "sbc": null, "sbs": null, "condition": null, "gq": null, "iscon": false, "ASMID": "ASM_296870", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }, { "min": "0", "max": "100", "decimal": false, "after_dec": "0", "id": "296871", "pt": "How many OOS were reported on the last call?", "answer": null, "group": "115601", "type": "NUMBER", "qtype": "Numeric", "last_ans": "", "act": "1", "sbc": null, "sbs": null, "condition": null, "gq": null, "iscon": false, "ASMID": "ASM_296871", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }, { "min": "0", "max": "50", "decimal": false, "after_dec": "0", "id": "296872", "pt": "How many secondary displays are on the sales floor?", "answer": null, "group": "115601", "type": "NUMBER", "qtype": "Numeric", "last_ans": "", "act": "1", "sbc": null, "sbs": null, "condition": null, "gq": null, "iscon": false, "ASMID": "ASM_296872", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }, { "values": [{ "id": "0", "pt": "Yes" }, { "id": "1", "pt": "No" }, { "id": "2", "pt": "NA" }], "id": "296873", "pt": "Has the store contacts been updated?", "answer": null, "group": "115601", "type": "PICK", "qtype": "YNNA", "last_ans": "", "act": "1", "sbc": "0", "sbs": "", "condition": null, "gq": "324626", "iscon": false, "ASMID": "ASM_296873", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }, { "values": [{ "id": "0", "pt": "Yes" }, { "id": "1", "pt": "No" }, { "id": "2", "pt": "NA" }], "id": "296874", "pt": "Does store management know the Advantage/ SCJ RSM?", "answer": null, "group": "115601", "type": "PICK", "qtype": "YNNA", "last_ans": "", "act": "1", "sbc": "0", "sbs": "", "condition": null, "gq": "324627", "iscon": false, "ASMID": "ASM_296874", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }, { "min": "0", "max": "50", "decimal": false, "after_dec": "0", "id": "296875", "pt": "ow many task were created during this audit for RSM follow up?", "answer": null, "group": "115601", "type": "NUMBER", "qtype": "Numeric", "last_ans": "", "act": "1", "sbc": null, "sbs": null, "condition": null, "gq": null, "iscon": false, "ASMID": "ASM_296875", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }, { "values": [{ "id": "407056", "pt": "Exceeds" }, { "id": "407057", "pt": "Meets" }, { "id": "407058", "pt": "Below" }], "id": "296876", "pt": "Are store conditions to expectations?", "answer": null, "group": "115601", "type": "PICK", "qtype": "Pick List", "last_ans": "", "act": "1", "sbc": null, "sbs": null, "condition": null, "gq": null, "iscon": false, "ASMID": "ASM_296876", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }, { "id": "296877", "pt": "Follow up notes", "answer": null, "group": "115601", "type": "TEXT", "qtype": "Text", "last_ans": "", "act": "1", "sbc": null, "sbs": null, "condition": null, "gq": null, "iscon": false, "ASMID": "ASM_296877", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }, { "values": [{ "id": "0", "pt": "Yes" }, { "id": "1", "pt": "No" }, { "id": "2", "pt": "NA" }], "id": "296878", "pt": " Are store notes being properly entered in this store?", "answer": null, "group": "115601", "type": "PICK", "qtype": "YNNA", "last_ans": "", "act": "1", "sbc": "0", "sbs": "", "condition": null, "gq": "324631", "iscon": false, "ASMID": "ASM_296878", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }, { "values": [{ "id": "0", "pt": "Yes" }, { "id": "1", "pt": "No" }, { "id": "2", "pt": "NA" }], "id": "296879", "pt": " Is the RS sending an audit follow up email directly after this call and copying ROM?", "answer": null, "group": "115601", "type": "PICK", "qtype": "YNNA", "last_ans": "", "act": "1", "sbc": "0", "sbs": "", "condition": null, "gq": "324632", "iscon": false, "ASMID": "ASM_296879", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }], "cm": "192", "ct": "457", "iscon": false, "atch": "", "URL": "" } } }, "promotions": { "folder": {} }, "pt": "Miscellaneous" }, { "id": "18", "dept": "13 Household Chemicals", "ord": "5", "distribution_folder": {}, "tasks": { "folder": [] }, "surveys": { "folder": { "129487": { "pt": "FOCO Glade Fall Air Care", "id": "129487", "folder": [{ "values": [{ "id": "489257", "pt": "Yes" }, { "id": "489258", "pt": "No" }], "id": "333853", "pt": "Does this store have a Glade Air Care display up supporting FOCO? If yes include photo.", "answer": null, "group": "129487", "type": "PICK", "qtype": "Pick List", "last_ans": "", "act": "0", "sbc": null, "sbs": null, "condition": null, "gq": null, "iscon": false, "ASMID": "ASM_333853", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }, { "values": [{ "id": "489259", "pt": "Up upon arrival" }, { "id": "489260", "pt": "Built during call (record units built in promo or merch)" }], "id": "333854", "pt": "Was the display up in the store upon arrival or built during the call?", "answer": null, "group": "129487", "type": "PICK", "qtype": "Pick List", "last_ans": "", "act": "0", "sbc": null, "sbs": null, "condition": null, "gq": null, "iscon": false, "ASMID": "ASM_333854", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }, { "values": [{ "id": "489261", "pt": "Fall Air Care" }, { "id": "489262", "pt": "Glade Air Care Standard" }, { "id": "489263", "pt": "Fall - Standard Combo" }], "id": "333855", "pt": "What type of Air Care display is up?", "answer": null, "group": "129487", "type": "PICK", "qtype": "Pick List", "last_ans": "", "act": "0", "sbc": null, "sbs": null, "condition": null, "gq": null, "iscon": false, "ASMID": "ASM_333855", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }, { "values": [{ "id": "489264", "pt": "SCJ Corp Display in original state (has SCJ POS)" }, { "id": "489265", "pt": "Other SCJ Corp Display (component used on endcap, sidewing)" }, { "id": "489266", "pt": "RSM Built Display (store inventory used)" }], "id": "333856", "pt": "Is the display a SCJ Corporate Display or was it built by RSM using open stock inventory?", "answer": null, "group": "129487", "type": "PICK", "qtype": "Pick List", "last_ans": "", "act": "0", "sbc": null, "sbs": null, "condition": null, "gq": null, "iscon": false, "ASMID": "ASM_333856", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }, { "values": [{ "id": "489267", "pt": "Endcap" }, { "id": "489268", "pt": "In- Aisle" }, { "id": "489269", "pt": "Perimeter of Store (not endcap)" }, { "id": "489270", "pt": "Seasonal Set" }], "id": "333857", "pt": "Where is the display located?", "answer": null, "group": "129487", "type": "PICK", "qtype": "Pick List", "last_ans": "", "act": "0", "sbc": null, "sbs": null, "condition": null, "gq": null, "iscon": false, "ASMID": "ASM_333857", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }, { "values": [{ "id": "489271", "pt": "Display Sold Through" }, { "id": "489272", "pt": "Product Filled to Shelf" }, { "id": "489273", "pt": "Manager Refused" }, { "id": "489274", "pt": "No Product Found" }], "id": "333858", "pt": "Reason a FOCO display is not up upon exit of this store?", "answer": null, "group": "129487", "type": "PICK", "qtype": "Pick List", "last_ans": "", "act": "0", "sbc": null, "sbs": null, "condition": null, "gq": null, "iscon": false, "ASMID": "ASM_333858", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "192", "reqPH": false, "hqp": false }], "cm": "192", "ct": null, "iscon": false, "atch": "", "URL": "" } } }, "promotions": { "folder": {} }, "pt": "Air Sanitizers - Fresheners - Candles" }],
            SCJ_SuggOrderSelectedFilter: [{ "Value": "", "Display": "All" }, { "Value": "Selected", "Display": "Selected Only" }],

            SCJ_DisplayDistribution_Info1: [{ "Source": "Data Source: Retail Link Week Ending 02-14-17" }],

            SCJ_DisplayDistribution_All1: [{ "Displays": "D13 Galde Atmosphere Spring TV End Cap", "ArrivalDate": "2/20/2017", "Qty": 1 }, { "Displays": "D13 Galde Atmosphere Spring TV End Cap", "ArrivalDate": "2/20/2017", "Qty": 1 }, { "Displays": "D13 Galde Atmosphere Spring TV End Cap", "ArrivalDate": "2/20/2017", "Qty": 1 }, { "Displays": "D13 Galde Atmosphere Spring TV End Cap", "ArrivalDate": "2/20/2017", "Qty": 1 }, { "Displays": "D13 Galde Atmosphere Spring TV End Cap", "ArrivalDate": "2/20/2017", "Qty": 1 }, { "Displays": "D13 Galde Atmosphere Spring TV End Cap", "ArrivalDate": "2/20/2017", "Qty": 1 }, { "Displays": "D13 Galde Atmosphere Spring TV End Cap", "ArrivalDate": "2/20/2017", "Qty": 1 }, { "Displays": "D13 Galde Atmosphere Spring TV End Cap", "ArrivalDate": "2/20/2017", "Qty": 1 }, { "Displays": "D13 Galde Atmosphere Spring TV End Cap", "ArrivalDate": "2/20/2017", "Qty": 1 }, { "Displays": "D13 Galde Atmosphere Spring TV End Cap", "ArrivalDate": "2/20/2017", "Qty": 1 }, { "Displays": "D13 Glade 216ct  3ct PISO Half Pallet", "ArrivalDate": "1/30/2017", "Qty": 1 }, { "Displays": "Windex- Scrubbing Bubbles Spring Train 1/2 Pallet Mix", "ArrivalDate": "2/13-3/13", "Qty": 1 }],

            SCJ_SSOOff_All1: [{ "City": "Altamonte Springs", "Pallet1": 0, "Pallet2": 0, "Pallet3": 1, "Pallet4": 1, "Pallet5": 1, "Pallet6": 0, "Pallet7": "", "Pallet8": "", "Pallet9": "", "Pallet10": "" },{ "City": "Altamonte Springs", "Pallet1": 1, "Pallet2": 0, "Pallet3": 0, "Pallet4": "", "Pallet5": "", "Pallet6": "", "Pallet7": "", "Pallet8": "", "Pallet9": "", "Pallet10": "" },{ "City": "Altamonte Springs", "Pallet1": "", "Pallet2": "", "Pallet3": 0, "Pallet4": "", "Pallet5": "", "Pallet6": "", "Pallet7": "", "Pallet8": "", "Pallet9": "", "Pallet10": "" }],

            SCJ_SSOOff_All1_Info1: [{ "Source": "Retail Link - 9/27/2016" }],

            SCJ_SSOGlade_All1 : [{"Pallet1":1,"Pallet2":"","Pallet3":"","Pallet4":"","Pallet5":"","Pallet6":"","Pallet7":"","Pallet8":"","Pallet9":"","Pallet10":""},{"Pallet1":0,"Pallet2":"","Pallet3":"","Pallet4":"","Pallet5":"","Pallet6":"","Pallet7":"","Pallet8":"","Pallet9":"","Pallet10":""}],

            SCJ_SSOGlade_Info1: [{"Source":"HyVee 2016 Data","LastUpdated":"6/29/2017"}],

            SCJ_SSOGlade_Price_All1: [{ "key": "Glade Fall Candle and PISO Display", "retail": "155.48", "profit": "56.24", "value": "" }],

            SCJ_SuperValue_Price_All1: [{ "key": "Ziploc Holiday Freezer/Storage 4 Core Display", "retail": "287.04", "profit": "94.75" }, { "key": "Ziploc Holiday Freezer/Storage Valu Bag Display", "retail": "233.48", "profit": "66.44" }, { "key": "Ziploc Holiday Container Display", "retail": "225", "profit": "74.81" }, { "key": "Ziploc Holiday Storage Slider Bag Display", "retail": "128", "profit": "37.15" }, { "key": "Glade Holiday Solid/Aero Display", "retail": "203.4", "profit": "156.93" }, { "key": "Glade Holiday Candles-Wax-PISO Display", "retail": "183.06", "profit": "68.1" }],

            SCJ_SSOSuperValu_All1: [{ "Display1": 3, "Display2": 2, "Display3": 2, "Display4": 4, "Display5": 2, "Display6": 2 }],

            SCJ_SSOSuperValu_Info1: [{ "Source": "Last Updated: 09/01/17" }],

            SCJ_FoodDisplayDistribution_All: [{ "Displays": "16PCZPLC BTS SAND SN", "ArrivalDate": "8/7/2017", "Qty": 1 }, { "Displays": "Glade 3ct Oil Half Pallet", "ArrivalDate": "8/3/2017", "Qty": 1 }, { "Displays": "Ziploc BTS Sandwich PDQ", "ArrivalDate": "8/7/2017", "Qty": 1 }, { "Displays": "Ziploc BTS 160pc Backpack", "ArrivalDate": "8/3/2017", "Qty": 5 }, { "Displays": "Glade September ECTV", "ArrivalDate": "8/21/2017", "Qty": 1 }, { "Displays": "September Candle Wax Half Pallet", "ArrivalDate": "8/21/2017", "Qty": 1 }, { "Displays": "Ziploc Holiday", "ArrivalDate": "10/9/2017", "Qty": 1 }, { "Displays": "Ziploc Holiday", "ArrivalDate": "10/23/2017", "Qty": 1 }, { "Displays": "Glade Oct $4.88 Half Pallet", "ArrivalDate": "9/25/2017", "Qty": 1 }],

            SCJ_FoodDisplayDistribution_Info1: [{ "Source": "Last updated 11/01/17" }],

            SCJ_FSIFOOD_All1: [{"Store":"Glade","StartDate":"10/15/2017","EndDate":"","Description":"Atmosphere Collection Product","Value":"$1/1"},{"Store":"Glade","StartDate":"10/15/2017","EndDate":"","Description":"Plugins Car Starter Kit","Value":"$1.5/1"},{"Store":"Glade","StartDate":"10/15/2017","EndDate":"","Description":"Glade Products (excludes Glade PlugIns Warmers, 8 oz. Room Sprays and Solid Air Fresheners)","Value":"$1/2"},{"Store":"Glade","StartDate":"10/15/2017","EndDate":"","Description":"Glade PlugIns Warmers, 8 oz. Room Sprays and Solid Air Fresheners","Value":"$2/3"},{"Store":"Glade","StartDate":"10/29/2017","EndDate":"11/4/2017","Description":"Car Starter Kit (Any)","Value":"$1.5/1"},{"Store":"Glade","StartDate":"10/29/2017","EndDate":"11/4/2017","Description":"Car Refills (Any)","Value":"$1/2"},{"Store":"Glade","StartDate":"10/29/2017","EndDate":"11/4/2017","Description":"(Any Every Day Home)","Value":"$2/3"},{"Store":"Glade","StartDate":"10/29/2017","EndDate":"11/4/2017","Description":"(Any Every Day Home)","Value":"$1/2"},{"Store":"Windex","StartDate":"10/29/2017","EndDate":"11/18/2017","Description":"Coupons.com","Value":"$0.5/1"},{"Store":"Shout","StartDate":"10/29/2017","EndDate":"11/18/2017","Description":"(Any) Coupons.com","Value":"$0.5/1"},{"Store":"Pledge","StartDate":"10/29/2017","EndDate":"11/18/2017","Description":"(Any Furniture) Coupons.com","Value":"$0.5/1"},{"Store":"Scrubbing Bubbles","StartDate":"10/29/2017","EndDate":"11/18/2017","Description":"Bath Coupons.com","Value":"$0.5/1"},{"Store":"Scrubbing Bubbles","StartDate":"10/29/2017","EndDate":"11/18/2017","Description":"Toilet Coupons.com","Value":"$0.75/1"},{"Store":"Drano","StartDate":"10/29/2017","EndDate":"11/18/2017","Description":"Coupons.com","Value":"$0.5/1"},{"Store":"fantastik","StartDate":"10/29/2017","EndDate":"11/18/2017","Description":"Coupons.com","Value":"$0.5/1"},{"Store":"Ziploc","StartDate":"10/29/2017","EndDate":"11/25/2017","Description":"Bags (Any) Coupons.com","Value":"$1/2"},{"Store":"Ziploc","StartDate":"10/29/2017","EndDate":"11/25/2017","Description":"Containers (Any) Coupons.com","Value":"$1/2"},{"Store":"Windex","StartDate":"10/29/2017","EndDate":"11/18/2017","Description":"Coupons.com","Value":"$0.5/1"},{"Store":"Windex","StartDate":"10/29/2017","EndDate":"11/18/2017","Description":"Coupons.com","Value":"$1.5/2"},{"Store":"Scrubbing Bubbles","StartDate":"10/29/2017","EndDate":"11/18/2017","Description":"Toilet Coupons.com","Value":"$2.50/2"},{"Store":"Ziploc","StartDate":"11/1/2017","EndDate":"","Description":"Ziploc Brand Bags","Value":"$1/2"},{"Store":"Ziploc","StartDate":"11/1/2017","EndDate":"","Description":"Ziploc Brand Containers","Value":"$1/2"},{"Store":"Glade","StartDate":"11/5/2017","EndDate":"11/18/2017","Description":"Car Starter Kit (Any)","Value":"$1.5/1"},{"Store":"Glade","StartDate":"11/5/2017","EndDate":"11/18/2017","Description":"Car Refills (Any)","Value":"$1/2"},{"Store":"Glade","StartDate":"11/5/2017","EndDate":"11/18/2017","Description":"(Any Fall Seasonal)","Value":"$1/2"},{"Store":"Glade","StartDate":"11/5/2017","EndDate":"11/18/2017","Description":"(Any Fall Seasonal)","Value":"$2/3"},{"Store":"Glade","StartDate":"11/5/2017","EndDate":"11/18/2017","Description":"Atmosphere Collection (Any) Quotient Network","Value":"$1/1"},{"Store":"Kiwi","StartDate":"11/5/2017","EndDate":"2/3/2018","Description":"Cleaners (Any) Coupons.com","Value":"$2/1"},{"Store":"Kiwi","StartDate":"11/5/2017","EndDate":"2/3/2018","Description":"Protectors (Any) Coupons.com","Value":"$2/1"},{"Store":"Glade","StartDate":"11/12/2017","EndDate":"","Description":"Car Starter Kit","Value":"$1.5/1"},{"Store":"Glade","StartDate":"11/12/2017","EndDate":"","Description":"Car Refills","Value":"$1/2"},{"Store":"Glade","StartDate":"11/12/2017","EndDate":"","Description":"(Any)","Value":"$1/2"},{"Store":"Glade","StartDate":"11/12/2017","EndDate":"","Description":"(Any)","Value":"$2/3"},{"Store":"Glade","StartDate":"11/12/2017","EndDate":"","Description":"Atmosphere Collection (Any)","Value":"$1/1"},{"Store":"Ziploc","StartDate":"11/12/2017","EndDate":"","Description":"(Any)","Value":"$1/1"},{"Store":"Windex","StartDate":"11/12/2017","EndDate":"","Description":"Windex?, fantastik?, Shout? (Any)","Value":"$0.5/1"},{"Store":"Drano","StartDate":"11/12/2017","EndDate":"","Description":"(Any)","Value":"$0.75/1"},{"Store":"Scrubbing Bubbles","StartDate":"11/12/2017","EndDate":"","Description":"Bath","Value":"$0.5/1"},{"Store":"Scrubbing Bubbles","StartDate":"11/12/2017","EndDate":"","Description":"Toilet","Value":"$1/1"},{"Store":"Glade","StartDate":"11/12/2017","EndDate":"","Description":"(Any - Excludes Plug-Ins Warmers, 8oz Aerosols & Solids)","Value":"$2/3"},{"Store":"Glade","StartDate":"11/12/2017","EndDate":"","Description":"(Any) Large Auto Refills and Sense & Spray Refills","Value":"$1/2"},{"Store":"Glade","StartDate":"11/12/2017","EndDate":"","Description":"PISO Twin Pack Refills","Value":"$1/1"},{"Store":"Glade","StartDate":"11/12/2017","EndDate":"","Description":"PISO, Candles and Wax Melts","Value":"$1/2"},{"Store":"Scrubbing Bubbles","StartDate":"11/19/2017","EndDate":"12/9/2017","Description":"Bath Coupons.com","Value":"$0.5/1"},{"Store":"Scrubbing Bubbles","StartDate":"11/19/2017","EndDate":"12/9/2017","Description":"Toilet Coupons.com","Value":"$1/1"},{"Store":"Windex","StartDate":"11/19/2017","EndDate":"12/9/2017","Description":"(Any) Coupons.com","Value":"$0.5/1"},{"Store":"Drano","StartDate":"11/19/2017","EndDate":"1/6/2018","Description":"Coupons.com","Value":"$0.5/1"},{"Store":"Drano","StartDate":"11/19/2017","EndDate":"12/24/2017","Description":"Coupons.com","Value":"$0.5/1"},{"Store":"Windex","StartDate":"11/19/2017","EndDate":"12/24/2017","Description":"(Excludes Trial & Travel Sizes) Coupons.com","Value":"$0.5/1"},{"Store":"Ziploc","StartDate":"12/1/2017","EndDate":"","Description":"Ziploc Brand Bags","Value":"$1/2"},{"Store":"Ziploc","StartDate":"12/1/2017","EndDate":"","Description":"Ziploc Brand Containers","Value":"$1/2"},{"Store":"Drano","StartDate":"12/3/2017","EndDate":"","Description":"(Any)","Value":"$1/1"},{"Store":"Glade","StartDate":"12/10/2017","EndDate":"","Description":"Atmosphere Collection (Any)","Value":"$1/1"},{"Store":"Glade","StartDate":"12/10/2017","EndDate":"","Description":"Car Starter Kit","Value":"$1.5/1"},{"Store":"Glade","StartDate":"12/10/2017","EndDate":"","Description":"Car Refills","Value":"$1/2"},{"Store":"Glade","StartDate":"12/10/2017","EndDate":"","Description":"Large Auto Refills and Sense & Spray Refills (Any)","Value":"$1/2"},{"Store":"Glade","StartDate":"12/10/2017","EndDate":"","Description":"PISO, Candles and Wax Melts","Value":"$1/2"},{"Store":"Glade","StartDate":"12/10/2017","EndDate":"","Description":"(Any - Excludes Plug-Ins Warmers, 8oz Aerosols & Solids)","Value":"$2/3"},{"Store":"Ziploc","StartDate":"12/10/2017","EndDate":"","Description":"Bags (Any) or Containers (Any)","Value":"$1/2"}],
                //[{ "FSIPromo": "Ziploc", "StartDate": "10/29/2017", "EndDate": "11/25/2017", "Description": "Containers (Any) Coupons.com", "Value": "$1/2" }, { "FSIPromo": "Windex", "StartDate": "10/29/2017", "EndDate": "11/18/2017", "Description": "Coupons.com", "Value": "$0.5/1" }, { "FSIPromo": "Windex", "StartDate": "10/29/2017", "EndDate": "11/18/2017", "Description": "Coupons.com", "Value": "$1.5/2" }, { "FSIPromo": "Scrubbing Bubbles", "StartDate": "10/29/2017", "EndDate": "11/18/2017", "Description": "Toilet Coupons.com", "Value": "$2.50/2" }, { "FSIPromo": "Ziploc", "StartDate": "11/1/2017", "EndDate": "", "Description": "Ziploc Brand Bags", "Value": "$1/2" }, { "FSIPromo": "Ziploc", "StartDate": "11/1/2017", "EndDate": "", "Description": "Ziploc Brand Containers", "Value": "$1/2" }, { "FSIPromo": "Glade", "StartDate": "11/5/2017", "EndDate": "11/18/2017", "Description": "Car Starter Kit (Any)", "Value": "$1.5/1" }, { "FSIPromo": "Glade", "StartDate": "11/5/2017", "EndDate": "11/18/2017", "Description": "Car Refills (Any)", "Value": "$1/2" }, { "FSIPromo": "Glade", "StartDate": "11/5/2017", "EndDate": "11/18/2017", "Description": "(Any Fall Seasonal)", "Value": "$1/2" }, { "FSIPromo": "Glade", "StartDate": "11/5/2017", "EndDate": "11/18/2017", "Description": "(Any Fall Seasonal)", "Value": "$2/3" }, { "FSIPromo": "Glade", "StartDate": "11/5/2017", "EndDate": "11/18/2017", "Description": "Atmosphere Collection (Any) Quotient Network", "Value": "$1/1" }, { "FSIPromo": "Kiwi", "StartDate": "11/5/2017", "EndDate": "2/3/2018", "Description": "Cleaners (Any) Coupons.com", "Value": "$2/1" }, { "FSIPromo": "Kiwi", "StartDate": "11/5/2017", "EndDate": "2/3/2018", "Description": "Protectors (Any) Coupons.com", "Value": "$2/1" }, { "FSIPromo": "Glade", "StartDate": "11/12/2017", "EndDate": "", "Description": "Car Starter Kit", "Value": "$1.5/1" }, { "FSIPromo": "Glade", "StartDate": "11/12/2017", "EndDate": "", "Description": "Car Refills", "Value": "$1/2" }, { "FSIPromo": "Glade", "StartDate": "11/12/2017", "EndDate": "", "Description": "(Any)", "Value": "$1/2" }, { "FSIPromo": "Glade", "StartDate": "11/12/2017", "EndDate": "", "Description": "(Any)", "Value": "$2/3" }, { "FSIPromo": "Glade", "StartDate": "11/12/2017", "EndDate": "", "Description": "Atmosphere Collection (Any)", "Value": "$1/1" }, { "FSIPromo": "Ziploc", "StartDate": "11/12/2017", "EndDate": "", "Description": "(Any)", "Value": "$1/1" }, { "FSIPromo": "Windex", "StartDate": "11/12/2017", "EndDate": "", "Description": "Windex®, fantastik®, Shout® (Any)", "Value": "$0.5/1" }, { "FSIPromo": "Drano", "StartDate": "11/12/2017", "EndDate": "", "Description": "(Any)", "Value": "$0.75/1" }, { "FSIPromo": "Scrubbing Bubbles", "StartDate": "11/12/2017", "EndDate": "", "Description": "Bath", "Value": "$0.5/1" }, { "FSIPromo": "Scrubbing Bubbles", "StartDate": "11/12/2017", "EndDate": "", "Description": "Toilet", "Value": "$1/1" }, { "FSIPromo": "Glade", "StartDate": "11/12/2017", "EndDate": "", "Description": "(Any - Excludes Plug-Ins Warmers, 8oz Aerosols & Solids)", "Value": "$2/3" }, { "FSIPromo": "Glade", "StartDate": "11/12/2017", "EndDate": "", "Description": "(Any) Large Auto Refills and Sense & Spray Refills", "Value": "$1/2" }, { "FSIPromo": "Glade", "StartDate": "11/12/2017", "EndDate": "", "Description": "PISO Twin Pack Refills", "Value": "$1/1" }, { "FSIPromo": "Glade", "StartDate": "11/12/2017", "EndDate": "", "Description": "PISO, Candles and Wax Melts", "Value": "$1/2" }, { "FSIPromo": "Scrubbing Bubbles", "StartDate": "11/19/2017", "EndDate": "12/9/2017", "Description": "Bath Coupons.com", "Value": "$0.5/1" }, { "FSIPromo": "Scrubbing Bubbles", "StartDate": "11/19/2017", "EndDate": "12/9/2017", "Description": "Toilet Coupons.com", "Value": "$1/1" }, { "FSIPromo": "Windex", "StartDate": "11/19/2017", "EndDate": "12/9/2017", "Description": "(Any) Coupons.com", "Value": "$0.5/1" }, { "FSIPromo": "Drano", "StartDate": "11/19/2017", "EndDate": "1/6/2018", "Description": "Coupons.com", "Value": "$0.5/1" }, { "FSIPromo": "Drano", "StartDate": "11/19/2017", "EndDate": "12/24/2017", "Description": "Coupons.com", "Value": "$0.5/1" }, { "FSIPromo": "Windex", "StartDate": "11/19/2017", "EndDate": "12/24/2017", "Description": "(Excludes Trial & Travel Sizes) Coupons.com", "Value": "$0.5/1" }, { "FSIPromo": "Ziploc", "StartDate": "12/1/2017", "EndDate": "", "Description": "Ziploc Brand Bags", "Value": "$1/2" }],

            SCJ_FSIFOOD_Info1: [{ "Source": "Last updated 11/01/17" }],

            SCJ_FSIFOOD_All1_SelectedFilter: [{ "Value": "", "Display": "All" }, { "Value": "Selected", "Display": "Selected Only" }],

            //SCJ_hiddenslides_all1: [{ "SlideName": "Pest", "ButtonPosition": 2, "TabPosition": 1, "PagePosition": 1 }, { "SlideName": "Pest", "ButtonPosition": 2, "TabPosition": 1, "PagePosition": 2 }, { "SlideName": "Pest", "ButtonPosition": 2, "TabPosition": 1, "PagePosition": 3 }, { "SlideName": "Pest", "ButtonPosition": 2, "TabPosition": 1, "PagePosition": 4 }],

        };
        data.STORENUMBER = "2081";//set the key to storenumber 2081 which matches the PWS_Opp_BusinessReview2 match value.
        data.PWS_Movement_Radio = [{
            "Value": "(WeekNum>40 && WeekNum<53)",
            "Display": "Last 12 weeks"
        }, { "Value": "(WeekNum>0 && WeekNum<13)", "Display": "LY Perf 12 weeks out" }];
        append_pitch_static_data(data); //call to/from the ASMCustomData.js		
        display_presentation_data(data);
        setup_presentation();
    }

    if (!testing) include('corescripts/SFPresentations.js');

    function display_presentation_data(data_hash_table) {
        crm_allow_save_data = data_hash_table.crm_allow_save_data;
        crm_current_call_key = data_hash_table.crm_current_call_key;
        crm_current_presentation_key = data_hash_table.crm_current_presentation_key;
        check_storage_clean_up(crm_current_call_key, crm_current_presentation_key);


        pitch_data = data_hash_table;
        
        //check if there is a data feed that will hide slides
        checkPitchDataForSlideAvailabilityDataFeed();

        /*
        *   The functions below will be called 
        *       core_setup_presentation();
        *       populate_ui_data($('body'));
        *   within the succeeding function above.
        *   --- checkPitchDataForSlideAvailabilityDataFeed();
        */

        /*
        *   you'll also need to run
        *   setupLayoutSetting(); here  on line 7
        *   fx is found on index.html
        */

        /*
        *   You'll need to add the hidden slides 
        *   table for this pitch
        */

        //addErrorLoggingButton();
        //core_setup_presentation();
        //populate_ui_data($('body'));
    }


    function display_presentation_data_error(message) {
        alert(message);
    }

    function setup_presentation(presentationID) {
        current_presentation_id = presentationID;
        var $end = $('.EndPresentation');
        $end.click(function () {
            var $video = $('video');
            if ($video.length > 0) {
                $video.get(0).pause();
            }


            core.record_breadcrumb('Presentation Ended', function () {
                SFPresentationAPI.end_presentation(presentationID, function () {
                    console.log('presentation ended');
                }, function (message) {
                    alert('Presentation end error: ' + message);
                });
            });
        });
        var $video = $('video');
        $('video').bind('play', function () {
            core.record_breadcrumb('Play Video: ' + $(this).get(0).currentSrc);
        });
        $('video').bind('pause', function () {
            core.record_breadcrumb('Pause Video: ' + $(this).get(0).currentSrc);
        });
        $('video').bind('ended', function () {
            core.record_breadcrumb('End Video: ' + $(this).get(0).currentSrc);
        });
    }

    function wait_for_presentations() {
        if ('SFPresentationAPI' in window) {
            console.log('setting up sfpresentation api');
            SFPresentationAPI.ready(function (presentationID) {
                SFTouch.log('presentation API ready');
                var data_success = function (data) {
                    append_pitch_static_data(data); //call to/from the ASMCustomData.js
                    current_presentation_id = presentationID;
                    display_presentation_data(data);
                    setup_presentation(presentationID);
                };
                SFPresentationAPI.get_presentation_data(presentationID, data_success, display_presentation_data_error);
            });
        }
        else {
            console.log('waiting for SFPresentationAPI');
            setTimeout(wait_for_presentations, 1000);
        }
    }

    if (testing)
        $(testingStartup);
    else
        wait_for_presentations();
})();


/////////////////////////////////////////////////////////////////
/// DATA DRIVEN SLIDE AVAILABILITY FUNCTIONS   [MGRC02/2017]
/////////////////////////////////////////////////////////////////

function checkPitchDataForSlideAvailabilityDataFeed() {
    console.log
    var pitchMap = jQuery.extend(true, {}, page_settings),
        slidesList = [],
        subString = 'hidden',
        dataSrc = '';

    for (var prop in pitch_data) {
        dataSrc = prop;
      

        dataSrc = dataSrc.toString().toLowerCase();
        //console.log(dataSrc);
        if (dataSrc.includes(subString.toLowerCase())) {
            var myDataFeed = prop;
        }
    }

    slidesList = pitch_data[myDataFeed];
   
    /*  If data feed[hide slides data] is 
    *   not present, go straight to checking
    *   slides that have tables which are  
    *   tagged to be hidden if they have no data */

    slidesList ? getTargetSlidesTabsButtons(pitchMap, slidesList) : checkTablesForData(pitchMap);

}

function getTargetSlidesTabsButtons(pitchMap, slidesList) {
    console.log('getTargetSlidesTabsButtons');
    //sort slidesArray in order of the last slide to hidden to first
    slidesList = sortTable(slidesList);
    console.log(slidesList);

    //Create Array with Names of Buttons Tabs and Slides
    var targetsArry = [],
        hiddenSlides = [],
        noData = false,
        buttonText;

    for (var i = 0; i < slidesList.length; i++) {

        var buttonPos = Number((slidesList[i].ButtonPosition)) - 1,
            tabPos = Number((slidesList[i].TabPosition)) - 1,
            pagePos = Number((slidesList[i].PagePosition)) - 1,
            sldName = slidesList[i].SlideName, //SLideName for Status Table
            //TARGET BUTTON
            myBtnAry = Object.keys(pitchMap.buttons),
            buttonName = myBtnAry[buttonPos],
            buttonTxt = pitchMap.buttons[buttonName].button_text; //Get Button Name
        if (sldName === undefined || sldName === "") {
            sldName = "N/A";
        }

        if (buttonName === undefined) {
            continue;
        }

        //TARGET TAB
        var myTabName = pitchMap.buttons[myBtnAry[buttonPos]].tabs[tabPos],
            myTabsArry = pitchMap.buttons[myBtnAry[buttonPos]].tabs,
            myTrgtTab = pitchMap.tabs[myTabName]; //Get TargetTab
            
        console.log(myTrgtTab);
            tabText = myTrgtTab.tab_title; //Get Button Name
        if (myTabName === undefined) {
            continue;
        }

        //TARGET SLIDE
        var myPgeArry = pitchMap.tabs[myTabName].pages,
            myTrgtPge = myPgeArry[pagePos];
        if (myPgeArry === undefined) {
            continue;
        }

        insertButtonTabSlideData(myTrgtPge, noData, buttonTxt, tabText, pagePos + 1, sldName);

        //Delete Slide from tab
        myPgeArry.splice(pagePos, 1);
        //If TAB page:Array == 0 delete this TAB
        if (myPgeArry.length === 0) {
            myTabsArry.splice(tabPos, 1);
        }

        //TAB length == 0 delete the BUTTON where this TAB belongs to
        if (myTabsArry.length === 0) {
            delete pitchMap.buttons[buttonName];
        }
    }

    checkTablesForData(pitchMap);
}

function checkTablesForData(pitchMap) {
    var idsArray = [];
    /*
    * Check Slides [*that aren't included in the Hidden Slides data feed] 
    * for tables that are tagged to be hidden if table has no data.
    */
    $('table[checkdataavailability]').each(function () {
        var myData = $(this).attr("data");
        if (!pitch_data[myData] || pitch_data[myData] == []) {
            var myParentDiv = $(this)[0].parentElement.parentNode.id,
                noData = true;
            try {
                insertButtonTabSlideData(myParentDiv, noData);
                idsArray.unshift(myParentDiv);
            } catch (err) {
                console.log(err);

                /*
                *   Add code block below when throwing and logging 
                *   errors Pitchbook functionality is ready
                */

                //var myContents = {};
                //myContents.statusText = err.message;
                //appendErrorToLog2(myContents);
            }
        }
    });

    idsArray.length != 0 ? removeTabsButtons(idsArray, pitchMap) : addStatusButton(idsArray, pitchMap);

}

function removeTabsButtons(idsArray, pitchMap) {

    var newPitchMap = jQuery.extend(true, {}, pitchMap),
        myTabs = newPitchMap.tabs;
    for (var i = 0; i < idsArray.length; i++) {
        for (var prop in newPitchMap.tabs) {
            var thisTab = newPitchMap.tabs[prop];
            for (var x = 0; x < thisTab.pages.length; x++) {

                if (thisTab.pages[x] == idsArray[i]) {
                    thisTab.pages.splice(x, 1);             // Remove Tab from Button
                }

                if (thisTab.pages.length === 0) {           // If there are no more slides 
                    var tabTitle = thisTab.tab_title;       // in the tab delete that tab 
                    delete newPitchMap.tabs[prop];          // then check to delete that button
                    findButton(newPitchMap, prop);          // if it has no more tabs in it.
                }
            }
        }
    }

    addStatusButton(idsArray, newPitchMap);
}

function addStatusButton(idsArray, newPitchMap) {

    var myArray = pitch_data.HiddenSlidesStatus;
    if (myArray != undefined || idsArray.length !== 0) {

        var obj = {
            button_text: 'Hidden Slides',
            tabs: ['HiddenSlides'],
            image: 'opportunity_icon.png'
        };

        var pitchNavButtons = addButton(newPitchMap, 'PitchSlideStatus', obj, function (pk, pv, nk, nv) {
            return (nk == 'Exit');
        });

        newPitchMap.buttons = pitchNavButtons;

        var myTab = {
            HiddenSlides: {
                tab_title: 'Hidden Slides',
                pages: ['SCJSSO_AirCareGlade2']
            }
        };
        $.extend(newPitchMap.tabs, myTab);
    }


    presentationSetup(newPitchMap);

}

function addButton(object, newKey, newVal, fn, bind) {
    var prevKey = null,
        prevVal = null,
        inserted = false,
        newobject = {};
    for (var prop in object) {
        if (prop == 'buttons') {
            for (var currKey in object[prop]) {
                if (object[prop].hasOwnProperty(currKey)) {
                    var currVal = object[prop][currKey];
                    if (!inserted && fn.call(bind, prevKey, prevVal, currKey, currVal)) {
                        newobject[newKey] = newVal;
                        inserted = true;
                    }
                    newobject[currKey] = currVal;
                    prevKey = currKey;
                    prevVal = currVal;
                }
            }
        }
    }
    if (!inserted) newobject[newKey] = newVal;

    return newobject;
}



function findButton(tempPages, tabTitle) {
    for (var key in tempPages.buttons) {
        var myTabs = tempPages.buttons[key].tabs;
        var myButton = tempPages.buttons[key];
        if (myTabs === undefined) return;
        for (var i = 0; i < myTabs.length; i++) {
            if (myTabs[i] == tabTitle) {
                deleteButton(myButton, tempPages, key);
                continue;
            }
        }
    }
}


function deleteButton(myButton, tempPages, key) {
    var tabs = myButton.tabs;
    var counter = 0;
    for (var i = 0; i < tabs.length; i++) {
        var mytabTitle = tabs[i];
        if (tempPages.tabs[mytabTitle] !== undefined) {
            counter = counter + 1;
        }
    }
    if (counter === 0) {
        delete tempPages.buttons[key];
    }
}

function insertButtonTabSlideData(slideID, noData, button, tab, slide, slideName) {

    var myArray = pitch_data.HiddenSlidesStatus;
    if (myArray === undefined) {
        pitch_data.HiddenSlidesStatus = [];
        myArray = pitch_data.HiddenSlidesStatus;
    }

    var myObj = {},
        mySlide = $('#' + slideID),
        btnName, tabName, sldNum, rsnHdn;

    /*
    * If slide has a table in it and was tagged to be hidden 
    * ---- table attribute [checkdataavailability="true"] ----
    * and if there's no no data for it the block of code below
    * will run
    */
    if (noData) {

        var slides = jQuery.extend(true, {}, page_settings);

        for (var prop in slides.tabs) {
            var pages = slides.tabs[prop].pages;
            for (var i = 0; i < pages.length; i++) {
                if (slideID == pages[i]) {
                    sldNum = i + 1;
                    tabName = slides.tabs[prop].tab_title;
                    slideName = slides.tabs[prop].tab_title;
                    tab = prop;
                    continue;
                }
            }
        }

        for (var prprty in slides.buttons) {
            var btnTabs = slides.buttons[prprty].tabs;
            if (prprty === "Exit") continue;
            for (var x = 0; x < btnTabs.length; x++) {
                if (btnTabs[x] === tab) {
                    //   console.log(btnTabs[x]);
                    btnName = slides.buttons[prprty].button_text;
                    continue;
                }
            }
        }

        rsnHdn = "No data available";

    } else {
        btnName = button;
        tabName = tab;
        sldNum = slide;
        rsnHdn = "Opportunity not available in this store";
    }

    myObj.button = btnName;
    myObj.tab = tabName;
    myObj.slide = sldNum;
    myObj.rsnHdn = rsnHdn;
    myObj.slideName = slideName

    myArray.push(myObj);
}

/*
*Function sorts array of slides in Descending order. Last slide in the array will be the first one to be hidden
*/
function sortTable(slidesList) {
    var helper = {};
    if (typeof helper == 'undefined') {
        helper = {};
    }

    helper.arr = {

        multisort: function (arr, columns, order_by) {
            if (typeof columns == 'undefined') {
                columns = [];
                for (x = 0; x < arr[0].length; x++) {
                    columns.push(x);
                }
            }

            if (typeof order_by == 'undefined') {
                order_by = [];
                for (x = 0; x < arr[0].length; x++) {
                    order_by.push('ASC');
                }
            }

            function multisort_recursive(a, b, columns, order_by, index) {
                var direction = order_by[index] == 'DESC' ? 1 : 0;

                var is_numeric = !isNaN(+a[columns[index]] - +b[columns[index]]);

                var x = is_numeric ? +a[columns[index]] : a[columns[index]].toLowerCase();
                var y = is_numeric ? +b[columns[index]] : b[columns[index]].toLowerCase();

                if (x < y) {
                    return direction === 0 ? -1 : 1;
                }

                if (x == y) {
                    return columns.length - 1 > index ? multisort_recursive(a, b, columns, order_by, index + 1) : 0;
                }

                return direction === 0 ? 1 : -1;
            }

            return arr.sort(function (a, b) {
                return multisort_recursive(a, b, columns, order_by, 0);
            });
        }
    };

    slidesList = (helper.arr.multisort(slidesList, ['ButtonPosition', 'TabPosition', 'PagePosition'], ['ASC', 'ASC'])).reverse();

    //for (var y = 0; y < slidesList.length; y++) {
    //    console.log(slidesList[y].ButtonPosition, slidesList[y].TabPosition, slidesList[y].PagePosition);
    //}

    return slidesList;
}


function presentationSetup(tempPages) {
    core_setup_presentation(tempPages);
    populate_ui_data($('body'));
}

/////////////////////////////////////////////////////////////////
/// DATA DRIVEN SLIDE AVAILABILITY FUNCTIONS END
/////////////////////////////////////////////////////////////////

































////////////////////////////////////////////////////////////////////
//  Adding Error Logging Button/Tab/Slide MGRC 3/2017
//function massagePalletData() {


//    var palletField = ["Pallet1", "Pallet2", "Pallet3", "Pallet4", "Pallet5", "Pallet6", "Pallet7", "Pallet8", "Pallet9", "Pallet10"]

//    $('div[dynamicpallets]').each(function (x) {

//        var data = $(this).attr("dynamicPallets"),
//            divId = $(this).attr('id'),
//            pallets = [],
//            storeCity = divId + "City",
//            myCity = [];

//        if (pitch_data[data] == undefined) { return };
//        var myPalletData = jQuery.extend(true, {}, pitch_data[data]);

//        myCity.push(myPalletData[0].City);
//        localStorage.setItem(storeCity, JSON.stringify(myCity));

//        for (var prop in myPalletData) {

//        }







//        //for (var x = 0; x < palletField.length; x++) {

//        //    for (var i = 0; i < Object.keys(myPallets).length; i++) {
//        //        //console.log(myPallets[i][palletField[x]]);
//        //        myVal = myPallets[i][palletField[x]];
//        //        if ($.isNumeric(myVal)) {
//        //            pallets.push(myVal);
//        //        }
//        //    }
//        //}

//        localStorage.setItem(divId, JSON.stringify(pallets));
//    });
//}

//function addErrorLoggingButton() {

//    var tempPages = jQuery.extend(true, {}, page_settings);

//    if (showLogErrors) {

//        var obj = {
//            button_text: 'Error Log',
//            tabs: ['ErrorLog'],
//            image: 'opportunity_icon.png'
//        };

//        var tempPages2 = addButton(tempPages, 'ErrorLog', obj, function (pk, pv, nk, nv) {
//            return (nk == 'Exit');
//        });

//        tempPages["buttons"] = tempPages2;

//        var myTab = {
//            ErrorLog: {
//                tab_title: 'Error Log',
//                pages: ['ErrorLog1']
//            }
//        };

//        $.extend(tempPages["tabs"], myTab);
//    }

//    //massagePalletData();
//    //presentationSetup(tempPages);
//}

//function addButton(object, newKey, newVal, fn, bind) {

//    var prevKey = null, prevVal = null, inserted = false, newobject = {};
//    for (var prop in object) {
//        if (prop == 'buttons') {
//            for (var currKey in object[prop]) {
//                if (object[prop].hasOwnProperty(currKey)) {
//                    var currVal = object[prop][currKey];
//                    if (!inserted && fn.call(bind, prevKey, prevVal, currKey, currVal)) {
//                        newobject[newKey] = newVal;
//                        inserted = true;
//                    }
//                    newobject[currKey] = currVal;
//                    prevKey = currKey;
//                    prevVal = currVal;
//                }
//            }
//        }
//    }
//    if (!inserted) newobject[newKey] = newVal;

//    return newobject;
//}


//function presentationSetup(tempPages) {
//    core_setup_presentation(tempPages);
//    populate_ui_data($('body'));
//}
///////////////////////////////////////////////////////////////////