﻿function populate_table($table, json_objects, match_value) {
    console.log('populate_table');
    var start = new Date();
    var rows, $col, $row, $cells, $cell, $columnsHeader, value, arrResults, match_color, tableId = $table.attr('id');

    $table.find('tbody > tr').remove();
    $columnsHeader = $table.find('tr.header > th');
    rows = [];

    /*match_color = $table.attr('style');*/
    match_color = $table.attr('matchRowColor');
    if (match_color == '')
        match_color == null;

    var tableWidth = 0;
    var columnArray = [];
    $columnsHeader.each(function (index) {
        $col = $(this);

        /*Builds the colObject and populates the variables with data based on attributes*/
        var colObject = {};

        colObject.field = $col.attr('field');
        colObject.filter_attribute = $col.attr('filterattribute');
        colObject.column_color = $col.attr('columncolor'); //gets the color for the table column
        colObject.format_mask = $col.attr('formatmask');
        colObject.row_content = $col.attr('rowcontent');
        colObject.LockedHeadersPaddingDifference = $col.attr('LockedHeadersPaddingDifference');
        colObject.width = $col.width();
        colObject.$column = $col;
        colObject.columnClass = $col.attr('class'); //col array
        colObject.tracking = $col.attr('columntracking'); //mars tracking

        columnArray.push(colObject);
    });
    var row;
    var widths = $table.attr('widths');
    if (widths != null)
        widths = $table.attr('widths').split(',');

    $.map(json_objects, function (item) {

        row = [];
        row.push('<tr>');
        $.map(columnArray, function (colObject, index) {
            try {
                cellStyles = [];
                cell = [];
                cell.push('<td');
                cellStyles.push('style="');

                var field = colObject.field;
                var filter_attribute = colObject.filter_attribute;
                var column_color = colObject.column_color;
                var format_mask = colObject.format_mask;
                var row_content = colObject.row_content;
                var LockedHeadersPaddingDifference = parseInt(colObject.LockedHeadersPaddingDifference);
                var tracking = colObject.tracking;

                if (widths != null) {
                    colObject.$column.width(widths[index]);
                }

                if (isNaN(LockedHeadersPaddingDifference)) {
                    cellStyles.push('width:' + (colObject.$column.width()) + 'px;');
                    cellStyles.push('min-width:' + (colObject.$column.width()) + 'px;');
                    cellStyles.push('max-width:' + (colObject.$column.width()) + 'px;');
                } else {
                    cellStyles.push('width:' + (colObject.$column.width() + LockedHeadersPaddingDifference) + 'px;');
                    cellStyles.push('min-width:' + (colObject.$column.width() + LockedHeadersPaddingDifference) + 'px;');
                    cellStyles.push('max-width:' + (colObject.$column.width() + LockedHeadersPaddingDifference) + 'px;');
                }

                if (rows.length == 0)
                    tableWidth += colObject.$column.width();

                if (field != null) {
                    value = eval_with_this(field, item);
                    change_color(value, cellStyles, colObject, row);
                    if (format_mask != null) {
                        value = scrubData(value);
                        change_color(value, cellStyles);
                        value = $.formatNumber(value, {
                            format: colObject.format_mask
                        });
                    }
                    cell.push('field="' + field + '"');
                } else {
                    value = '';
                    field = '';
                }
                if (filter_attribute != null) {
                    cell.push(filter_attribute + '=""'); //$cell.attr(filterattribute,'');
                }


                /*Colors the rows for tables Mars tracking and Demo Team Distric Comp*/
                //console.log(item);

                if (match_value != null && match_value == item.MatchCondition && match_color != null) {
                    cellStyles.push('background-color:' + match_color + ';');
                }
                    //Checks to see if coloring value for Mars tracking
                else if (column_color != null && tracking == "on") {
                    if (value != "") {
                        cellStyles.push('background-color:' + column_color + ';');
                    } else {
                        //skip dont color cell if it has no value
                    }
                }
                    //Colors DEMO team District Comp table
                else if (column_color != null) {
                    cellStyles.push('background-color:' + column_color + ';');
                }

                //append the cell details if there are row_content from the header
                if (row_content != null) {
                    value = value + row_content;
                }

                cellStyles.push('"');
                cell.push(cellStyles.join(' '));
                cell.push('>' + value + '</td>');
                row.push(cell.join(' '));
            } catch (ex) { }
        });
        //if (rows.length<20){
        row.push('</tr>');
        rows.push(row.join(' '));
        //}
    });

    showTimeDiff(start, 'populated rows');
  
    var skipwidth = $table.attr('skipwidthcalculation')
    if (!skipwidth)
        $table.width(tableWidth);

    $table.find('tbody').append(rows);
    //find all values with storagekeyprefix,
    var $elementsWithStoragesKeys = $table.find('*[storagekeyprefix]')
    $elementsWithStoragesKeys.each(function (index) {
        var $item = $(this), $itemUPC = $item.parent().parent()[0].children[2].innerText; //TODO: dynamically capturing UPC
        //append an unique index behind the key prefix make sure everything is unique.
        $item.attr('storagekeyprefix', $item.attr('storagekeyprefix') + '' + index);
        //load from the storage and see if we have any OLD values defined and restore them into the checkbox.
        var value = load_storage($item.attr('storagekeyprefix'));


        /*Custom Code for Suggested Order Start
        *
        *   add classes for input tags
        *   suggOrder1 & suggOrder
        *   
        */
        if ($item.hasClass('suggOrder')) {
            var myIndex = stripNonRealDigits($item[0].className),
                myVal = retrieveJSON(myIndex, tableId, $itemUPC);
            if ($item.is('[type=checkbox]')) {
                if (myVal == true || myVal == 'true') {
                    $item.attr('checked', true);
                }
            } else if ($item.is('[type=tel]')) {
                if (myVal) {
                    $item.attr('value', myVal);
                } else {
                    $item.attr('value', " ");
                }
            }
        }
        /*
        *     Custom Code for Suggested Order END              
        */
        else {
            if ($item.is('[type=checkbox]')) {
                $item.attr('checked', value ? (value.toLowerCase() == 'true' || value === true) : false);
            } else if ($item.is('[type=textbox]')) {               
                $item.attr('value', value);
            } else if ($item.is('[type=tel]')) {             
                $item.attr('value', value);
            } else if ($item.is('[type=number]')) {               
                $item.attr('value', value);
            } else if ($item.hasClass('checkboximage')) {
                $item.toggleClass('checked', value ? (value.toLowerCase() == 'true' || value === true) : false);
            } else if ($item.is('p')) {
                $item.html(value);
            }
        }
    });

    $table.on('change', 'input[storagekeyprefix]', function () {
        var $item = $(this), $itemUPC = $item.parent().parent()[0].children[2].innerText, //TODO: dynamically capturing UPC 
            $numOfInpts = $item.parent().parent().find('input').length;

        //  Suggested Order Code that creates the local storage item
        //  and Creates key/value pairs depending on the number of inputs
        if ($item.hasClass('suggOrder')) {
            jsonCreate($numOfInpts, $itemUPC, tableId)
        }

        // inputs For  [SUGGESTED ORDER]
        /*
        *   Custom Code for Suggested Order START
        */
        if ($item.hasClass('suggOrder')) {
            var myKey = stripNonRealDigits($item[0].className);

            if ($item.is('[type=checkbox]')) { // for items that are dynamically created                
                //var isChecked = $item.attr('checked', $item.is(':checked'));
                //console.log('suggOrder checkbox');
                saveItems(tableId, $itemUPC, myKey, $item.is(':checked'))
                save_storage($item.attr('storagekeyprefix'), $item.is(':checked'));

            } else if ($item.is('[type=tel]')) {//for items that are dynamically created
                $item.attr('value', $item.val());

                saveItems(tableId, $itemUPC, myKey, $item.val())
                save_storage($item.attr('storagekeyprefix'), $item.val());
            }
            /*
            *   Custom Code for Suggested Order END
            */
            //normal tables
        } else if ($item.is('[type=checkbox]')) {
            $item.attr('checked', $item.is(':checked'));
            save_storage($item.attr('storagekeyprefix'), $item.is(':checked'));
        } else if ($item.is('[type=textbox]')) { //enable textbox.
            //alert('table change' + ' : ' + $item.attr('storagekeyprefix') + ' : ' + $item.attr('class') + ' : ' + $item.val());
            $item.attr('value', $item.val());
            save_storage($item.attr('storagekeyprefix'), $item.val());
        } else if ($item.is('[type=tel]')) { //enable textbox.
            //alert('table change' + ' : ' + $item.attr('storagekeyprefix') + ' : ' + $item.attr('class') + ' : ' + $item.val());
            $item.attr('value', $item.val());
            save_storage($item.attr('storagekeyprefix'), $item.val());
        } else if ($item.is('[type=number]')) { //enable textbox.
            //alert('table change' + ' : ' + $item.attr('storagekeyprefix') + ' : ' + $item.attr('class') + ' : ' + $item.val());
            $item.attr('value', $item.val());
            save_storage($item.attr('storagekeyprefix'), $item.val());
        }

    });
    if (typeof populate_table_overload == 'function') {
        //alert($table[0].id);
        populate_table_overload($table[0].id);
    };


    //Colors the previous row of the table
    //colorPreviousRow($table);
    //colorNextRow($table);
    //Get row count of all rows in the table
    //rowCount(rows, $table);
}

function retrieveJSON(myIndex, tableId, UPC) {
    var key = tableId + "_" + UPC;
    //console.log(key)
    var myData = JSON.parse(localStorage.getItem(key));
    if (!myData) {
        return false;
    } else {
        return myData[0][myIndex];
    }
}

function jsonCreate(inputs, UPC, tableId) {
    var myKey = tableId + "_" + UPC, myJSON, myObj, myArray = [];

    myJSON = JSON.parse(localStorage.getItem(myKey));

    if (!myJSON) {
        myObj = {};
        for (var i = 0; i < inputs; i++) {
            var mykey = i + 1;
            myObj[mykey] = '';
        }

       //myObj= {1:'',2:''}
        //  myArray = []

        myArray.push(myObj);
        // [{1:'',2:''}]
        save_storage(myKey, JSON.stringify(myArray));
    }
}

function saveItems(tableId, UPC, key, value) {
    var myKey = tableId + "_" + UPC;
    var myJson = JSON.parse(load_storage(myKey));
    myJson[0][key] = value;
    localStorage.setItem(myKey, JSON.stringify(myJson));
}

function stripNonRealDigits(numberSource) {
    var m_strOut = new String(numberSource);
    m_strOut = m_strOut.replace(/[^\d.]/g, '');
    return m_strOut;
}

//{"tblSCJ_SuggestedOrder_All1_046500018428":[{"1":true,"2":""}]}

function colorNextRow($table) {

    var table = document.getElementById('tblMWM_KPI_All1');

    //start of by 1 to not include the header of the table
    for (var i = 1; i < table.rows.length; i++) {

        if (scrubDataToInt(table.rows[i].cells[1].innerHTML) <= scrubDataToInt(table.rows[i].cells[2].innerHTML)) {
            table.rows[i].cells[2].style.backgroundColor = "#B7cc5B";
        } else {
            table.rows[i].cells[2].style.backgroundColor = "#D81130";

        }
    }
}

//Color previous cells in the table.
//INPUT: $table
function colorPreviousRow($table) {

    //Find all the TD in the table
    var allTD = $table.find('tbody > tr > td');
    //Initialize color variable.
    var colors = "";
    //Loop through each TD, find its color,
    //color previous cell that color.
    for (var i = 0; i < allTD.length; i++) {
        //Holds one TD at a time.
        colors = allTD[i];
        if (colors.style.backgroundColor == "red") {
            allTD[i - 1].style.backgroundColor = "red";
            allTD[i - 1].style.color = "black";
        } else if (colors.style.backgroundColor == "yellow") {
            allTD[i - 1].style.backgroundColor = "yellow";
            allTD[i - 1].style.color = "black";
        } else if (colors.style.backgroundColor == "green") {
            allTD[i - 1].style.backgroundColor = "green";
            allTD[i - 1].style.color = "black";
        }
        //Reset color variable
        colors = "";
    }
}

//Gets the row count for the table
//INPUT: $table, rows
function rowCount(rows, $table) {
    //Get the row count.
    var count = rows.length;
    //Get the table ID
    var tableID = $table.attr("id");
    //Get the class of rowCountOn
    var rowCountOn = $table.attr("class");
    //Add the "Number of Gaps" to the table
    //Checks if the tableID is not null and if the rowCountOn
    //is added to the table.
    if (tableID != null && rowCountOn.indexOf("rowCountOn") != -1) {
        //Displays the row count of the table.
        $('#' + tableID).before('<div id="rowCountStyle">Number of Gaps: ' + count + '</div>');
    }
}

//Colors the text and columns of the tables
function change_color(value, cellStyles, colObject) {

    //colors the values text that are less then zero red
    if (value == null || cellStyles == null) return false;
    if (value < 0) {
        cellStyles.push('color:#FF0000;');
    }
    /*
     *Colors the columns if they have the class "colorColumn".
     */
    //Count of how many class in the table have "colorColumn"
    var count = document.getElementsByClassName("colorColumn");
    //initialize the variable.
    var colorColumnName = "";

    for (var i = 0; i < count.length; i++) {

        //gets the field value that has the class "colorColumn"
        colorColumnName += document.getElementsByClassName("colorColumn")[i].getAttribute("field");

        //colors the rows based on the cell values and makes the text color black.
        if (colObject.field.toString() == colorColumnName && colObject.columnClass != "") {
            if (value <= 0) {
                cellStyles.push('background-color: red;');
                cellStyles.push('color:black');
            } else if (value >= 1 && value <= 7) {
                cellStyles.push('background-color: yellow;');
                cellStyles.push('color:black');
            } else if (value > 7) {
                cellStyles.push('background-color:green;');
                cellStyles.push('color:black');
            }
        }

        //reset the variable
        colorColumnName = "";
    }
}




function display_barcode($tr, tableID) {
    //console.log("display_barcode");
    var UPCCode, ProductName, Product, tableNum, tmpImg = new Image(),
        UPCfield, targetUPC;

    $('table[displayBarcode]', $('#pageholder')).each(function (GT) {
        var Id = $(this).attr("id");
        if (Id == tableID) {
            GT = GT + 1;
            //UPCCode = "UPCCode" + GT;
            //ProductName = "ProductName" + GT;
            //Product = "Product" + GT;
            //tableNum = GT;
            //UPCfield = $(this);
            targetUPC = $tr[0].cells[2].innerHTML; //column where your UPC is
            nuBarCode = "nuBarCode" + GT;
        }
    });
   // console.log(targetUPC, tableNum);

    // UPCA.ShowBarcode($('div#' + UPCCode), targetUPC);
    //tmpImg.src = "./images/Products/" + targetUPC + ".png";

    makeBarCode(targetUPC, nuBarCode);

    // $('div#' + ProductName).text($tr.find('td[field="Product"]').text());
    //Quick fix for image resize. There to many images and no standard size.

    //tmpImg.onerror = function () {
    //    Prd = Product;
    //    $('div#' + Prd).html('<img height="150"  src="./images/No-image-found.jpg" style="position: relative; margin-left: auto; margin-right: auto; display: block;">');
    //};

    //███████████████████████████████████████████████████████
    //IMAGE for the product
    //███████████████████████████████████████████████████████

    //$(tmpImg).one('load', function () {
    //    orgWidth = tmpImg.width;
    //    orgHeight = tmpImg.height;

    //    if (orgHeight > 100) {
    //        tmpImg.height = 121;
    //        tmpImg.style.position = "relative";
    //        //tmpImg.style.top = "25px";
    //        //tmpImg.style.left = "48px";
    //        tmpImg.style.marginLeft = "auto";
    //        tmpImg.style.marginRight = "auto";
    //        tmpImg.style.display = "block";
    //    } else {
    //        if (orgWidth > 600) {
    //            tmpImg.width = 250;
    //            tmpImg.style.position = "relative";
    //            tmpImg.style.top = "47px";
    //            tmpImg.style.left = "31px";
    //        } else {
    //            tmpImg.style.position = "relative";
    //            tmpImg.style.top = "47px";bar
    //            tmpImg.style.left = "23px";
    //        }
    //    }
    //    $('div#' + Product).html(tmpImg);
    //});

    //$('div#Product').html('<img id="imageProperties" onerror="imageError()" src="./images/Products/' + $tr.find('td[field="UPC"]').text() + '.png">');
    $('.selected-info').width($('#tblSalesData').width());
}


function makeBarCode(upc, targetDiv) {
    console.log("make barcode main");
    // UPCA.ShowBarcode($('.cellBarcode' + index), upc);   
    try {
        $('.' + targetDiv).JsBarcode(upc, {
            format: "upc",
            width: 3,
            height: 150,
            fontSize: 45
        });
    }
    catch (err) {
        $('.' + targetDiv).attr('src', '');
        console.error(targetDiv, err)
        return;
    }
}



function changeRadioGraphFilter($radioContainer, item, all_data) {
    var $graphContainer = $('#' + $radioContainer.attr('filtertarget'));
    var graphData = eval_with_this($graphContainer.attr('graphdata'), all_data);
    var max_date;
    for (var x in graphData) {
        var record = graphData[x];
        var daysDiff, weekDiff;
        if (record.WeekNum == 52) {
            max_date = record.WeekEnding;
            daysDiff = Date.daysBetween(Date.parse(max_date), Date.today().moveToDayOfWeek(0, -1));
            weekDiff = parseInt(daysDiff / 7);
            break;
        }
    }
    if (weekDiff == null || weekDiff == 0)
        item.Value = 'WeekNum<13';
    else
        item.Value = '(WeekNum>=' + (1 + weekDiff) + ' && WeekNum<' + (13 + weekDiff) + ')';

    return item;
}

Date.daysBetween = function (date1, date2) {
    //Get 1 day in milliseconds
    var one_day = 1000 * 60 * 60 * 24;

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    var difference_ms = date2_ms - date1_ms;

    // Convert back to days and return
    return Math.round(difference_ms / one_day);
}

//function filterSelectedRow(arrString, $row) {
//    var selectedOnly = arrString[0] == 'Selected';
//    var hasChecked = $row.find('div.checkboximage.checked,input:checked').length > 0;
//    $row.find('div.checkboximage,input:checkbox').parent().toggleClass('hidden', !hasChecked && selectedOnly);
//}


function filterSelectedRow2(arrString, $row) {
    var selection = '';
    var hasChecked = $row[0].cells[0].childNodes[1].checked;

    if (arrString == 'Selected') {
        var selectedOnly = arrString[0] == 'Selected';
        $row.find('div.checkboximage,input:checkbox').parent().toggleClass('hidden', !hasChecked && selectedOnly);
        selection = 'Selected';
    } else if (arrString == 'Unselected') {



        var selectedOnly = arrString[0] == 'Unselected';
        $row.find('div.checkboximage,input:checkbox').parent().toggleClass('hidden', hasChecked && selectedOnly);
        selection = 'Unselected';



    } else {
        $row.find('div.checkboximage,input:checkbox').parent().toggleClass('hidden', hasChecked && !hasChecked);
        selection = 'All';
    }

    //recalculateTotals($row, selection);
}


function filterSelectedRow(arrString, $row) {
    //console.log(arrString, $row);
    var hasChecked = $row.find('div.checkboximage.checked,input:checked').length > 0;

    if (arrString == 'Selected') {
        var selectedOnly = arrString[0] == 'Selected';
        $row.find('div.checkboximage,input:checkbox').parent().toggleClass('hidden', !hasChecked && selectedOnly);

    } else if (arrString == 'Unselected') {
        var selectedOnly = arrString[0] == 'Unselected';
        $row.find('div.checkboximage,input:checkbox').parent().toggleClass('hidden', hasChecked && selectedOnly);
    } else {
        $row.find('div.checkboximage,input:checkbox').parent().toggleClass('hidden', hasChecked && !hasChecked);
    }

}



function scrubData(value) {
    if (isNaN(value)) {
        return value = value.replace('$', '').replace('%', '').replace(',', '').replace('(', '').replace(')', '');
    } else {
        return value = value.toString();
    }
}

function scrubDataToInt(value) {
    if (isNaN(value)) {

        value = value.replace('$', '').replace('%', '').replace(',', '').replace('(', '').replace(')', '');
        value = parseInt(value);
        return value
    } else {

        value = parseInt(value);
        return value;
    }
}