function runSlideFunctions($table) {

    var Tid = $table.attr('id'),
        targetTable, tblCalcFunction;
    $("#" + Tid + " " + 'table[tablecalcfunction]').each(function (index, el) {
        targetTable = $(this);
        tblCalcFunction = window[targetTable.attr('tablecalcfunction')];
    });

    if (tblCalcFunction !== undefined) {
        setTimeout(function () {
            tblCalcFunction(targetTable);
        }, 500);
    }

    ////Run CustomPhoto Function that loads photos
    if (document.getElementById($table.attr('id')).hasAttribute("loadPhotos")) {
        console.log("loadPhotos");
        //loadSlidePhotos(LSKey); 
    }

    //LOAD local storage items for inputs that are not in tables
    if (document.getElementById($table.attr('id')).hasAttribute("loadLocalStorageItems")) {
        var slide = document.getElementById($table.attr('id'));
        loadLocalStorageItems(Tid);
    }

    if (document.getElementById($table.attr('id')).hasAttribute("loaditems")) {
        var $true = true;
        BuildHTMLPDFTable($table, $true);
    }
    //console.log($row.find('div.checkboximage,input:checkbox').parent());

}



function calculateLostSales(targetTable) {
    var Tid = targetTable.attr('id')

    $('input', $('td', $("tr", $('#' + Tid + " " + 'tbody')))).each(function () {

        var myVal = load_storage($(this).attr('storagekeyprefix'));
        console.log('from calculateLostSales', myVal)
        let myclass = $(this).prop('className');

        if (myclass.includes('suggOrder') || $(this).hasClass('suggOrder')) {
            if ($(this).is('[type=checkbox]')) {
                if (myVal === 'true' || myVal === true) {
                    console.log($(this))
                    $(this).attr('checked', true);
                } else if (myVal === 'false' || myVal === false) {
                    $(this).attr('checked', false);
                }
            } else if ($(this).is('[type=tel]')) {
                if (myVal) {
                    $(this).attr('value', myVal);
                } else {
                    $(this).attr('value', " ");
                }
            }
        }
    });

}


function SelectAll(checkID, tableId) {
    let isChecked = false;
    if (checkID.checked == true) isChecked = true;

    //var inputs = document.getElementsByTagName('input');
    //for (var i = 0; i < inputs.length; i++) {
    //    if (inputs[i].type == 'checkbox') {
    //        inputs[i].checked = checkID.checked;
    //    }
    //} 

    let $inputCheckboxes = $('#' + tableId).find('*[storagekeyprefix]');
    $inputCheckboxes.each(function (index) {
        let myJSON, myArray = [];
        this.checked = isChecked;

        let $Key = $(this).attr('storagekeyprefix');
        console.log('storagekeyprefix from select all', $Key)
        save_storage($Key, isChecked);

        let itemIndex = stripNonRealDigits($(this)[0].className)
        let myCells = $($($(this).prop('parentNode')).prop('parentNode')).prop('cells')
        let UPC = $($.grep(myCells, function (a) { if ($(a).attr("field") === 'UPC') { return $(a) } })).prop('innerHTML');
        let myKey = tableId + "_" + UPC;

        myJSON = JSON.parse(localStorage.getItem(myKey));
        console.log(myJSON)
        if (!myJSON) {           
            let myObj = {};
            myObj[myKey] = '';
            myArray.push(myObj);
            save_storage(myKey, JSON.stringify(myArray));
        }
     
        var myJson = JSON.parse(load_storage(myKey));
        myJson[0][itemIndex] = isChecked;
        localStorage.setItem(myKey, JSON.stringify(myJson));
    });

    var sKey = $(checkID).attr('storagekeyprefix'),
        userInputVal = checkID.checked;

        save_storage(sKey, userInputVal);
}

function ClearAll(container_name, recalculate_table) {
    if (container_name === null)
        container_name = $(event.srcElement).parent().attr('id');

    var $container;
    $container = $('#' + container_name);

    var $multiSelects = $container.find('select[multiselect=true][storagekeyprefix]');
    $multiSelects.multiselect("uncheckAll");
    $multiSelects.change();
    var $Selects = $container.find('select[multiselect=false][storagekeyprefix]');
    $Selects.val('');
    $Selects.change();

    var $elementsWithStoragesKeys = $container.find('*[storagekeyprefix]');
    $elementsWithStoragesKeys.each(function (index) {

        var $item = $(this), $itemUPC = $item.parent().parent()[0].children[2].innerText; //TODO: dynamically capturing UPC 

        /*Custom Code for Suggested Order
           *
           *   add classes for input tags in HTML
           *   i.e. suggOrder1 & suggOrder
           *   
           */
        if ($item.hasClass('suggOrder')) {
            if ($item.is('[type=checkbox]')) {
                console.log('suggOrder checkbox');
                var myIndex = stripNonRealDigits($item[0].className);
                clearSuggOrderJSONs(myIndex, recalculate_table, $itemUPC);
                $item.attr('checked', false);

                let $Key = $(this).attr('storagekeyprefix');
                if ($Key) {
                    save_storage($Key, false);
                }

            } else if ($item.is('[type=tel]')) {
                var myIndex = stripNonRealDigits($item[0].className);
                clearSuggOrderJSONs(myIndex, recalculate_table, $itemUPC);
                $item.attr('value', " ");
            }
        } else if ($item.is('[type=checkbox]')) {
            $item.attr('checked', false);
            $item.change();
        } else if ($item.hasClass('checkboximage')) {
            if ($item.hasClass('checked'))
                $item.click();
        } else if ($item.is('[type=textbox]')) {
            $item.val('');
            $item.change();
        }

        var $selectAllCheckBox = $container.find('[selectAllCheckBox]');
        if ($selectAllCheckBox) {
            $selectAllCheckBox.attr('checked', false);
        }        

    });
    //change this by calculator
    if (recalculate_table.substring(0, 6) == 'MLF_PC') {
        BigBetsRecalculateTable(recalculate_table);
    }

}


function loadLocalStorageItems(slideId) {
    //console.log('loadlocalstorageitems', slideId);
    var $elementsWithStoragesKeys = $('#' + slideId).find('*[storagekeyprefix]');
    //console.log('$elementsWithStoragesKeys', $elementsWithStoragesKeys);
    $elementsWithStoragesKeys.each(function (index) {
        var $item = $(this);
        //append an unique index behind the key prefix make sure everything is unique.
        $item.attr('storagekeyprefix');
        console.log('from loadLocalStorageItems', $item.attr('storagekeyprefix'));
        //load from the storage and see if we have any OLD values defined and restore them into the checkbox.
        var value = load_storage($item.attr('storagekeyprefix'));
        if ($item.is('[type=checkbox]')) {
            $item.attr('checked', value ? (value.toLowerCase() == 'true' || value === true) : false);
        } else if ($item.is('[type=tel]')) {
            $item.attr('value', value);
        } else if ($item.hasClass('checkboximage')) {
            $item.toggleClass('checked', value ? (value.toLowerCase() == 'true' || value === true) : false);
        }
    })

    //calculateTotalRetailValue(slideId, dSource);
}


// Suggested Order FX
function clearSuggOrderJSONs(myIndex, tableId, $itemUPC) {
    var key = tableId + "_" + $itemUPC;
    var myData = JSON.parse(localStorage.getItem(key));

    if (!myData) return;

    if (myIndex == 1) {
        myData[0][myIndex] = false;
    } else {
        myData[0][myIndex] = " ";
    }
    save_storage(key, JSON.stringify(myData));
}

$(window).ready(function () {
    $('body select[filterTarget]').bind('custom_multi_select', function () {
        //when any of those select values are changed, it will run this function
        //the selected values are returned within the arrSelected array.
        //to convert to string,  use the $.map function.
        var arrSelected = $(this).multiselect("getChecked");
        var arrString = $.map(arrSelected, function (item) {
            return item.value;
        });
        var filterDiv = $(this).closest("div").attr("id");
        if (filterDiv.substring(0, 6) == 'MLF_PC') {
            var tblDiv = 'tbl' + filterDiv.replace("_filters", "");
            BigBetsRecalculateTable(tblDiv);
        }
    });
})

function hydratePalletData($parent_ui) {
    var divId = $parent_ui[0].id, palletData, cityData;

    if (!document.getElementById(divId).hasAttribute("dynamicPallets")) {
        return;
    }

    palletData = JSON.parse(localStorage.getItem(divId));
    cityData = JSON.parse(localStorage.getItem(divId + "City"));

    if (palletData == null) { return };


    $('div[datalookup]', $('#' + divId)).each(function (x) {
        // console.log($(this));
        var myData = palletData[x];
        //console.log(myData);

        if (myData === undefined) {
            console.log(myData);
            myData = "n/a";
        }

        $(this)[0].innerHTML = myData;
    });
    $('div[cityData]', $('#' + divId)).each(function (x) {
        // console.log($(this));
        var myCData = cityData[x];

        if (myCData === undefined) {
            myCData = "n/a";
        }
        $(this)[0].innerHTML = myCData;
    });

}

function checkboxcheck(element, productName, upc, status, qty) {
    console.log($(element), productName, upc, status, qty)
    //
    //  Capture Product details //name, Upc, Status, QTY
    //

    var sNumber = pitch_data.STORENUMBER, key = "SuggOrder_CustomPDF" + sNumber,
        selectedProducts = getProducts(key);

    var count = $("#tblSCJ_SuggestedOrder_All1 input[type='checkbox']:checked").length;
    var $element = $(element);
    var checkbox = $(element.parentNode.parentNode.cells[0].childNodes[1]);
    var $productName = $(element.parentNode.parentNode.cells[productName]);
    //parentNode: td, parentNode: tr, cells[1]: column, childNodes: text
    var $upc = $(element.parentNode.parentNode.cells[upc]);
    var $status = $(element.parentNode.parentNode.cells[status]);
    var $qty = $(element.parentNode.parentNode.cells[qty]);
    console.log($qty)

    //console.log($($productName).prop('innerHTML'), $($upc).prop('innerHTML'), $($status).prop('innerHTML'), $qty);

    if (count <= 20) {
        if (checkbox.prop('checked') == true) {
            console.log(checkbox.prop('checked'))
            //check if in the array, if it is remove it or push it if not in array
            if ($.inArray($($upc).prop('innerHTML'), selectedProducts) == -1) {
                for (i = 0; i < selectedProducts.length; i++) {
                    if (selectedProducts[i].UPC == $($upc).prop('innerHTML')) {
                        selectedProducts.splice([i], 1);
                        console.log("deleting identical item");
                    }
                }
                console.log("pushing another element in the array", count);
                selectedProducts.push({
                    "product": $($productName).prop('innerHTML'),
                    "UPC": $($upc).prop('innerHTML'),
                    "status": $($status).prop('innerHTML'),
                    "Qty": $($qty).prop('innerHTML'),
                });
            }
        }
        if (checkbox.prop('checked') == false) {
            console.log(checkbox.prop('checked'))
            if (selectedProducts.length != 0) {
                for (i = 0; i < selectedProducts.length; i++) {
                    if (selectedProducts[i].UPC == $($upc).prop('innerHTML')) {
                        selectedProducts.splice([i], 1);
                    }
                }
            }
        }
        localStorage.setItem(key, JSON.stringify(selectedProducts));
    } else {
        checkbox.prop('checked', false);
        confirm("You cannot select more then 20 products!");
    }
}

function buildPDFData() {
    var targtTable = $("[buildPDFData]"), tableId = targtTable.prop('id'), selectedProducts = [];


    //console.log('targtTable', targtTable, 'tableId', tableId);

    // if (suffix === "IC") return;

    let TableRows = document.getElementById(tableId).getElementsByTagName('tbody')[0].rows.length,
        thisRow = 0;


    while (thisRow < TableRows) {
        var elmnt = document.getElementById(tableId).getElementsByTagName('tbody')[0].children[thisRow];

        if (!$(elmnt).hasClass('hidden') && $(elmnt).find('input:checkbox').is(':checked')) {

            // Adjust accordingly/tailor fit to what you want to show on your PDF export slide.
            var myTDs = document.getElementById(tableId).getElementsByTagName('tbody')[0].children[thisRow].children,
                productName = myTDs[1].childNodes[0].data;

            $upc = stripNonRealDigits($($.grep($(elmnt).prop('cells'), function (a) { if ($(a).attr("field") === 'UPC') { return $(a) } })).prop('innerHTML')),
                $thisNu$qty = $.grep($(elmnt).prop('cells'), function (a) { if ($(a).attr("field") === 'Qty') { return $(a) } }),
                $thisStatus = $.grep($(elmnt).prop('cells'), function (a) { if ($(a).attr("field") === 'Status') { return $(a) } })
            theRealDisplay$qty = Number(stripNonRealDigits($($thisNu$qty).find('input').prop('innerHTML')));

            console.log($thisStatus, $($thisStatus).prop('innerHTML'))
            //console.log($thisNu$qty, theRealDisplay$qty);
            selectedProducts.push({
                "product": productName,
                "UPC": $upc,
                "Qty": $($thisNu$qty).prop('innerHTML'),
                "status": $($thisStatus).prop('innerHTML'),
            });

        }
        thisRow++;
    }
    console.log('selectedProducts', selectedProducts);
    return selectedProducts;
}


function BuildHTMLPDFTable($table) {
    var sNumber = pitch_data.STORENUMBER, key = "SuggOrder_CustomPDF" + sNumber, iter = 0,
        targetData = getProducts(key), sugOrdItmsMax, targetcontainer;

    //console.log(targetData);
    //TARGET SLIDE to DISPLAY SUGGESTED ORDER DATA
    targetcontainer = $table.attr('loaditems');

    //Get the inner divs of the html template 1/2/3 etc... 
    var HTMLTemplate = $("." + targetcontainer).children();
    maxTblItms = HTMLTemplate.length;




    //Data Object
    var targetData = buildPDFData();
    //console.log(suffix);
    /*
    *maxTblItms = number of items that can be displayed 
    *in a suggested order summary slide
    */

    console.log(targetcontainer)
    //Clear the table
    $('.' + targetcontainer + ' ' + 'div').empty();

    /*  
    *maxTblItms is the number of items(suggested order) for each summmary slide
    *iter = is the start of iteration for the loop
    *targetLength = length of data array for slide1 or slide2 on summary slide/RANGE of data to loop on 
    */


    //console.log(iter, targetLength, targetcontainer, targetData, HTMLTemplate);

    for (var x = 0; x < targetData.length; x++) {
        var myCell = x >= maxTblItms ? x - maxTblItms : x;
        //console.log(x, myCell);
        HTMLTemplate[myCell].innerHTML =

            "<div class='summaryTable'>" +
            "<div class='cellProductName DCsummaryItem2'>"
            + targetData[x].product +
            "</div>" +
            "</div>" +

            "<div class='summaryTable'>" +
            "<div class='cellProductUPC DCsummaryItem2'>"
            + "UPC: " + targetData[x].UPC +
            "</div>" +
            "</div>" +

            "<div class='summaryTable'>" +
            "<div class='cellProductStatus DCsummaryItem2'>"
            + "Status: " + targetData[x].status +
            "</div>" +
            "</div>" +

            "<div class='summaryTable'>" +
            "<div class='cellProductQTY DCsummaryItem2'>"
            + "Qty: " + targetData[x].Qty +
            "</div>" +
            "</div>" +

            "<img class='cellBarcode cellBarcode" + x + "'>"
        makePDfBarCode(x, targetData[x].UPC);
    }
}

function makePDfBarCode(index, upc) {
    console.log("make barcode tool box");
    try {
        $('.cellBarcode' + index).JsBarcode(upc, {
            format: "upc",
            width: 1.5,
            height: 30,
            margin: 1
        });
    }
    catch (err) {
        return;
    }

}

function getProducts(key) {
    var myData = JSON.parse(localStorage.getItem(key));

    if (!myData || myData == null) {
        return [];
    } else {
        //console.log(myData.length);
        return myData;
    }
}


function stripNonRealDigits(numberSource) {
    var $TinaSaysItsWeird = new String(numberSource);
    return $TinaSaysItsWeird.replace(/[^\d.]/g, '');

}


//function ClearAll(container_name, recalculate_table) // This is a clear function athat wipes out saved values (whether checkbox, text box, or filter) and re-filters the table. Because some tables need to have special recalculation functions, we can activate custom functions by table-name.
//{
//    if (container_name === null)
//        container_name = $(event.srcElement).parent().attr('id');

//    var $container;
//    $container = $('#' + container_name);

//    //Gets all the highlighted rows in the table.
//    var tableCompleteRows = $container.children().children().children().children();

//    var $multiSelects = $container.find('select[multiselect=true][storagekeyprefix]');
//    $multiSelects.multiselect("uncheckAll");
//    $multiSelects.multiselect("close");
//    var $Selects = $container.find('select[multiselect=false][storagekeyprefix]');
//    $Selects.val('');
//    $Selects.change();

//    var $elementsWithStoragesKeys = $container.find('*[storagekeyprefix]');
//    $elementsWithStoragesKeys.each(function (index) {
//        var $item = $(this);
//        if ($item.is('[type=checkbox]')) {
//            $item.attr('checked', false);
//            //remove highlighted row
//            tableCompleteRows.removeClass('complete');
//            $item.change();
//        } else if ($item.hasClass('checkboximage')) {
//            if ($item.hasClass('checked'))
//                $item.click();
//        } else if ($item.is('[type=tel]')) {
//            $item.val('');
//            $item.change();
//        }
//    });
//}

//EvenListener that grey out the row that was clicked on
document.addEventListener("change", function (event) {
    //get the element that was clicked on
    var el = $(event.target);
    //get the TR that contains the checkbox
    var tableRow = el.closest("tr");
    if (el.attr("checked") == "checked") {
        tableRow.addClass("complete");
    }
    else {
        tableRow.removeClass("complete");
    }

});




//  
//  SUGGESTED ORDER START
//



function DynamicPitch_SetStringAndWrite(data_hash_table) {
    console.log('start');
    var Activities = data_hash_table['ARTS_Client_Activities'];

    if (data_hash_table['suggOrderLI'] == null) {
        data_hash_table['suggOrderLI'] = { 'LI': 0 }
    }

    if (data_hash_table['suggOrderOOS'] == null) {
        data_hash_table['suggOrderOOS'] = { 'OOS': 0 }
    }

    var ListString = '';
    var addString = false;
    //   if (typeof Activities[x].distribution_folder[i]['OS'] != "undefined" && typeof Activities[x].distribution_folder[i]['AC'] != "undefined") {
    for (var x = 0; x < Activities.length; x++) {
        for (var i in Activities[x].distribution_folder) { //Ok, now we are in each product.
            //check if OS exist in the object
            let auxQty = '';

            if (typeof Activities[x].distribution_folder[i]['OS'] !== "undefined") {
                if ((Activities[x].distribution_folder[i]['OS'].pt === "Out-Of-Stock (OOS)" || Activities[x].distribution_folder[i]['OS'].pt === "Low Inventory (LI)")) {

                    //if (Activities[x].distribution_folder[i]['AC'] == null) {
                    //    continue;
                    //}

                    addString = checkOpeningAndActionStatus(Activities[x].distribution_folder[i], data_hash_table);
                    //console.log(Activities[x].distribution_folder[i]);
                    Activities[x].distribution_folder[i]['aux']['Fields']['aux5'] !== undefined ? auxQty = Activities[x].distribution_folder[i]['aux']['Fields']['aux5'] : auxQty = 0;
                    if (addString) {
                        if (Activities[x].distribution_folder[i].pt) {
                            ListString += ',{"Product":"' + Activities[x].distribution_folder[i]['pt'] + '","UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '","Qty":"' + auxQty + '"}';
                        }
                        else if (Activities[x].distribution_folder[i].prompt) {
                            ListString += ',{"Product":"' + Activities[x].distribution_folder[i]['prompt'] + '","UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '","Qty":"' + auxQty + '"}';
                        }
                    }
                }
            }
        }//end Distribution Folder
    }//end Activities

    //Massage data quick and easy since client wants PF from Json displayed as EX 10373 -> Core 40
    ListString = ListString.replace(/11012/g, 'Front End');
    ListString = ListString.replace(/11013/g, 'In Aisle');

    ListString = ListString.replace(/Distribution Void \(DV\)/g, "DV");
    ListString = ListString.replace(/Out-Of-Stock \(OOS\)/g, 'OOS');
    //ListString = ListString.replace(/On Shelf \(SH\)/g, 'ORS');
    ListString = ListString.replace(/On Shelf Discontinue \(SD\)/g, 'SD');
    ListString = ListString.replace(/No Tag \(NT\)/g, 'NT');
    ListString = ListString.replace(/On Shelf \(SH\)/g, 'SH');
    ListString = ListString.replace(/Low Inventory \(LI\)/g, 'LI');



    if (ListString == '') {
        ListString = 'No Distribution items able to be listed.';
    }
    else {
        ListString = '{SugOrd_All1:[' + ListString + ']}';
        var OOSobj = eval("(" + ListString + ')');
    }

    //console.log("OOSobj", OOSobj);
    //check if obj there or crashs the app if there is no data for this data source.
    if (OOSobj != undefined) {
        //sort
        OOSobj.SugOrd_All1.sort(function (a, b) {
            // return (a.Product) - (b.Product);
            return (a.Product > b.Product) - (a.Product < b.Product)
        });
    }

    //merge dynamic object
    $.extend(true, data_hash_table, OOSobj);

    return data_hash_table;
}

function checkOpeningAndActionStatus(data, data_hash_table) {
    let currentNum, newNum;
    if (data['OS']['pt'] === "Out-Of-Stock (OOS)") {
        // console.log('Out-Of-Stock (OOS)', data);      
        if (typeof data['AC'] === 'undefined' || data['AC'] === null) {

            currentNum = $(data_hash_table['suggOrderOOS']).prop('OOS');
            newNum = currentNum + 1;

            $(data_hash_table['suggOrderOOS']).prop('OOS', newNum);

            //console.log(data_hash_table['suggOrderOOS']);
            return true;
        } else if (data['AC'].pt) { // 
            return false;
        }
    }

    if (data['OS']['pt'] === "Low Inventory (LI)") {
        // console.log('No Tag (NT)', data);        
        if (typeof data['AC'] === 'undefined' || data['AC'] === null) {

            currentNum = $(data_hash_table['suggOrderLI']).prop('LI');
            newNum = currentNum + 1;

            $(data_hash_table['suggOrderLI']).prop('LI', newNum);

            //console.log(data_hash_table['suggOrderLI']);
            return true;
        } else if (data['AC'].pt) { // if (data['AC'].pt)
            return false;
        }
    }


    return false;
    // Distribution Void (DV)
    // Out-Of-Stock (OOS) 
}



